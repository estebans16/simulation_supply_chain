var assert = require('assert');
var Conversation = require('../src/conversation');
var Message = require('../src/message');

describe('Conversation', function() {
  describe('#Validations', function() {
    beforeEach(function() {
      message = new Message(); 
      conversation = new Conversation({id: 1, messages: [message]});
    });
    
    it('should return valid', function() {
      assert.equal(conversation.isValid(), true);
    });
    
    it('should return invalid, undefined attributes', function() {
      conversation.id = undefined;
      assert.equal(conversation.isValid(), false);
      assert.equal(conversation.errors().isEmpty(), false);
      assert.equal(conversation.errors().fullMessage().toString().includes('missing the parameter id'), true);
    });

    it('should return invalid, empty attributes', function() {
      conversation.id = '';
      assert.equal(conversation.isValid(), false);
      assert.equal(conversation.errors().isEmpty(), false);
      assert.equal(conversation.errors().fullMessage().includes('the parameter id can not be blank'), true);
    });
  });
});