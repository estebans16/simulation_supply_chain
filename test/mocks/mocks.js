var AWS = require('aws-sdk-mock');

class Mocks {
	
	agentMocks(){
		// Create
		AWS.mock('DynamoDB.DocumentClient', 'put', function (params, callback){
			callback(null, {});
		});
		// Update
		let responseUpdate = { Attributes: {	chainId: 1, active: false,
    																		 	conversations: [], is_external: false, transactionNumber: 2,
     																			providers: [1, 2, 3],
     																			uri: 'http://localhost/agents/2',
     																			stock: {item: "etiquetas", price: 15, count: 20, reserved: 0, availableAtDate: new Date()},
     																			agentName: 'Fabrica de etiquetas2' 
     																		} 
     											};
   	AWS.mock('DynamoDB.DocumentClient', 'update', function (params, callback){
   		callback(null, responseUpdate);
    });
   	// Find
    let responseGet = { Item: {	chainId: 1, active: true,
														 		conversations: [], is_external: false, transactionNumber: 2,
																providers: [1, 2, 3],
																uri: 'http://localhost/agents/2',
																stock: {item: "etiquetas", price: 15, count: 20, reserved: 0, availableAtDate: new Date()},
																agentName: 'Fabrica de etiquetas2', agentType: 'SimpleBehavior',
																id: 2

     													}
    };
    AWS.mock('DynamoDB.DocumentClient', 'get', function (params, callback){
      callback(null, responseGet);
    });
	};

	chainMocks(){
		AWS.mock('DynamoDB.DocumentClient', 'put', function (params, callback){
      callback(null, {});
    });
    let responseUpdate = { Attributes: { description: ' ', template: false, 
                                     transactionNumber: 2, chainName: 'Cadena de prueba 2'
                                   } 
    };
    AWS.mock('DynamoDB.DocumentClient', 'update', function (params, callback){
      callback(null, responseUpdate);
    });

    let responseGet = { Item: { description: ' ', id: 7881107934117958, template: false,
                    transactionNumber: 9, chainName: 'Chain' }
    };

    AWS.mock('DynamoDB.DocumentClient', 'get', function (params, callback){
      callback(null, responseGet);
    });

    AWS.mock('DynamoDB.DocumentClient', 'scan', function (params, callback){
      callback(null, { Items: [], Count: 0, ScannedCount: 0 });
    });
	}

	allChainsMocks(){
		let response = { Items: 
		   [ { template: true,
		       description: ' ',
		       id: 7907125272379424,
		       transactionNumber: 1,
		       chainName: 'Cadena de test2' } ],
		  Count: 1,
		  ScannedCount: 1 
		}

		AWS.mock('DynamoDB.DocumentClient', 'scan', function (params, callback){
      callback(null, response);
    });	
	}

	allAgentsMocks(){
		let response = { Items: 
			[ 
				{ chainId: 7881107934117958,
	      	active: true,
		      conversations: [],
		      is_external: false,
		      transactionNumber: 10,
		      providers: [],
		      uri: 'https://x24cw30z27.execute-api.us-east-1.amazonaws.com/dev/agents/7881102434927053',
		      stock: {item: "etiquetas", price: 15, count: 20, reserved: 0, availableAtDate: new Date()},
		      id: 7881102434927053, agentType: 'SimpleBehavior',
		      agentName: 'mio' 
	     },
  		],
  		Count: 4,
  		ScannedCount: 4 
  	};
		AWS.mock('DynamoDB.DocumentClient', 'scan', function (params, callback){
      callback(null, response);
    });	
	}

	awsRestore(){
		AWS.restore();
	}
}

module.exports = Mocks;