var assert = require('assert');
var CourierLambda = require('../src/courierLambda');
var PersistDynamoDb = require('../src/persistDynamoDb');
var ItemStock = require('../src/itemStock');
var Agent = require('../src/agent');
var Message = require('../src/message');

describe('CourierLambda', function() {
  describe('#Validations', function() {
    beforeEach(function() {
        //Instancio agentes y persistDynamoDB
        let providers = [1, 2, 3];
        let stock = new ItemStock({item: "etiquetas", price: 15, count: 20, reserved: 0, availableAtDate: new Date()});
        let conversations = [];
        let agent = new Agent({agentName: "Fabrica de etiquetas", providers: providers, stock: stock, 
                            conversations: conversations, active: true , transactionNumber: null, 
                            id: null, chainId: 1, uri: null, is_external: false });
        let agent2 = new Agent({agentName: "Envases", providers: providers, stock: stock, 
                            conversations: conversations, active: true , transactionNumber: null, 
                            id: null, chainId: 1, uri: null, is_external: false });
        let persistDynamoDB = new PersistDynamoDb();
        persistDynamoDB.saveAgent(agent);
        persistDynamoDB.saveAgent(agent2);

        //Instancio courierLambda
        courierLambda = new CourierLambda();

        //Instancio un mensaje
        let someSenderUrl = "http://localhost/agents/1";
        let someReceiverUrl = "http://localhost/agents/2";
        let someContent = {count: 5};
        let someConversation = 1
        aMessage = new Message({sender: someSenderUrl, receiver: someReceiverUrl, content: someContent, performative: 'CallForProposal', conversationId: someConversation});
    });
    
    it('should push', function() {
        assert.equal(courierLambda.outbox.length, 0);
        courierLambda.queue(aMessage);
        assert.equal(courierLambda.outbox.length, 1);
    });

    it('should queue', function() {
        courierLambda.queue(aMessage);
        assert.equal(courierLambda.outbox.length, 1);
        courierLambda.deliverMessages();
        assert.equal(courierLambda.outbox.length, 0);
    });

  });

});