var assert = require('assert');
var Chain = require('../src/chain');
var Agent = require('../src/agent');

describe('Chain', function() {
  describe('#Validations', function() {
    beforeEach(function() {
      chainTemplate = new Chain({chainId: 1, chainName: "Cadena de suministro", description: "Cadena de una automotriz", template: true});
      chain = new Chain({chainId: 1, chainName: "Cadena de suministro", description: "Cadena de una automotriz"});

      clone = chainTemplate.clone();
      agent = new Agent();
    });
    
    it('should return valid', function() {
      assert.equal(chain.isValid(), true);
      assert.equal(clone.isValid(), true);
    });
    
    it('should return invalid, undefined attributes', function() {
      chain.id = undefined;
      assert.equal(chain.isValid(), false);
      assert.equal(chain.errors().isEmpty(), false);
      assert.equal(chain.errors().fullMessage().toString().includes('missing the parameter id'), true);
      assert.equal(chain.errors().fullMessage().toString().includes('missing the parameter id'), true);
    });
    
    it('should return invalid, agent invalid', function() {
      chain.agents = [agent];
      assert.equal(chain.isValid(), false);
      assert.equal(chain.errors().isEmpty(), false);
      assert.equal(chain.errors().fullMessage().toString().includes('missing the parameter agentName'), true);
    });



    it('should return invalid, empty attributes', function() {
      chain.id = '';
      assert.equal(chain.isValid(), false);
      assert.equal(chain.errors().isEmpty(), false);
      assert.equal(chain.errors().fullMessage().includes('the parameter id can not be blank'), true);
    });
  });
});