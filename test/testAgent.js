var assert = require('assert');
var ItemStock = require('../src/itemStock');
var Agent = require('../src/agent');

describe('Agent', function() {
  describe('#Validations', function() {
    beforeEach(function() {
      // constructor(name, providers, stock, conversations, active, transactionNumber=0, id, chainId, uri, is_external=false) {
      let providers = [1, 2, 3];
      let stock = new ItemStock({item: "etiquetas", price: 15, count: 20, reserved: 1, availableAtDate: new Date()});
      let conversations = [];
      let chainId = 1;
      let chainId2 = 2;
      agent = new Agent({agentName: "Fabrica de etiquetas", providers: providers, stock: stock, conversations: conversations, active: true , transactionNumber: null, id: null, chainId: chainId, uri: null, is_external: false, providersFull: null });

      clone = agent.clone();
      clone.chainId = chainId2;
    });
    
    it('should return valid', function() {
      assert.equal(agent.isValid(), true);
    });
    
    it('should return invalid, undefined attributes', function() {
      let agent = new Agent();
      assert.equal(agent.isValid(), false);
      assert.equal(agent.errors().isEmpty(), false);
      assert.equal(agent.errors().fullMessage().includes('missing the parameter agentName'), true);
    });

    it('should return invalid, empty attributes', function() {
      agent.id = '';
      assert.equal(agent.isValid(), false);
      assert.equal(agent.errors().isEmpty(), false);
      assert.equal(agent.errors().fullMessage().includes('the parameter id can not be blank'), true);
    });

    it('should return invalid, stock attribute item blank ', function() {
      agent.stock.item = '';
      assert.equal(agent.isValid(), false);
      assert.equal(agent.errors().isEmpty(), false);
      assert.equal(Object.keys(agent.errors().messages()).includes("ItemStock"), true)
      assert.equal(agent.errors().fullMessage().includes("item"), true)
    });
  });
});