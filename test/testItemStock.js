var assert = require('assert');
var ItemStock = require('../src/itemStock');

describe('ItemStock', function() {
  describe('#Validations', function() {
    it('should return valid', function() {
      var itemS = new ItemStock({item: "Tomates", price: 15, count: 20, reserved: 1, availableAtDate: new Date()});
      assert.equal(itemS.isValid(), true);
    });

    it('should return invalid, undefined attributes', function() {
      var itemS = new ItemStock();
      assert.equal(itemS.isValid(), false);
      assert.equal(itemS.errors().isEmpty(), false);
      assert.equal(itemS.errors().fullMessage().includes('missing the parameter item'), true);
      assert.equal(Object.keys(itemS.errors().messages()).includes("price"), true)
    });

    it('should return invalid, empty attributes', function() {
      var itemS = new ItemStock({item: '', price: '', count: '', reserved: '', availableAtDate: ''});
      assert.equal(itemS.isValid(), false);
      assert.equal(itemS.errors().isEmpty(), false);
      assert.equal(itemS.errors().fullMessage().includes('the parameter item can not be blank'), true);
    });
  });
});