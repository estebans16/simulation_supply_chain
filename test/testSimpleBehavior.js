var assert = require('assert');
var AgentSimpleBehavior = require('../src/subclasses_agent/SimpleBehavior');
var CourierInMemory = require('../src/courierInMemory');
// var PersistInMemory = require('../src/persistInMemory');
var Message = require('../src/message');

describe('Agent Simple Behavior', function() {
  describe('#Messajes', function() {
    beforeEach(function() {
      let providers = [1, 2, 3];
      let stockSend = {item: "Final", price: 15, count: 1, reserved: 1, availableAtDate: new Date()};
      let stockReceibes = {item: "Latas", price: 12, count: 100, reserved: 1, availableAtDate: new Date()};
      let conversations = [];
      let chainId = 1;
      agentSend = new AgentSimpleBehavior({agentName: "Cliente", providers: [],
                                                     stock: stockSend, conversations: conversations, active: true, 
                                                     transactionNumber: null, id: null, chainId: chainId, 
                                                     uri: null, is_external: false, providersFull: null });
      agentReceives = new AgentSimpleBehavior({agentName: "Consumidor", providers: [],
                                                     stock: stockReceibes, conversations: conversations, active: true, 
                                                     transactionNumber: null, id: null, chainId: chainId, 
                                                     uri: null, is_external: false, providersFull: null });
      courierInMemory = new CourierInMemory();
    });
    describe('#CallForProposal', function() {
      it('should return Propose', function() {
        // Preparon un cfp que tiene como respuesta un propose
        let cpfMessage = new Message({sender: agentSend.uri, receiver: agentReceives.uri, 
                                      content: {"item": 'Latas', "count": 20 }, 
                                      performative: 'CallForProposal', replyWith: 1234});
        agentReceives.addMessageToConversation(cpfMessage);
        assert.equal(courierInMemory.outbox.length, 0);
        agentReceives.processMessage(cpfMessage, courierInMemory);
        assert.equal(courierInMemory.outbox.length, 1);
        assert.equal(courierInMemory.outbox[0].performative, 'Propose');
        assert.equal(courierInMemory.outbox[0].inReplyTo, cpfMessage.replyWith);
        assert.equal(courierInMemory.outbox[0].content.price, 12);
      });

      it('should return refuse', function() {
        // Preparon un cfp que tiene como respuesta un propose
        let cpfMessage = new Message({sender: agentSend.uri, receiver: agentReceives.uri, 
                                      content: {"item": 'Latas', "count": 101 }, 
                                      performative: 'CallForProposal', replyWith: 1234});
        agentReceives.addMessageToConversation(cpfMessage);
        assert.equal(courierInMemory.outbox.length, 0);
        agentReceives.processMessage(cpfMessage, courierInMemory);
        assert.equal(courierInMemory.outbox.length, 1);
        assert.equal(courierInMemory.outbox[0].performative, 'Failure');
        assert.equal(courierInMemory.outbox[0].inReplyTo, cpfMessage.replyWith);
      });
    });

    describe('#Propose', function() {
       beforeEach(function() {
        // Conversacion iniciada
        cpfMessage = new Message({sender: agentSend.uri, receiver: agentReceives.uri, 
                                      content: {"item": 'Latas', "count": 20 }, 
                                      performative: 'CallForProposal', replyWith: 1234});
        agentReceives.addMessageToConversation(cpfMessage);
        firstPropose = new Message({sender: agentReceives.uri, receiver: agentSend.uri, 
                                      content: {"item": 'Latas', "count": 20, "price": 12, "period": 1 }, 
                                      performative: 'Propose', conversationId: cpfMessage.conversationId,
                                      inReplyTo: 1234, replyWith: 12345});
        rejectPropose =  new Message({sender: agentSend.uri, receiver: agentReceives.uri, 
                                      content: {"item": 'Latas', "count": 20, "price": 12, "period": 1 }, 
                                      performative: 'RejectProposal', conversationId: cpfMessage.conversationId,
                                      inReplyTo: 12345});
        agentReceives.addMessageToConversation(firstPropose);
        agentReceives.addMessageToConversation(rejectPropose);
        
       });

      it('should return AcceptProposal', function() {
        let secondProposeMessage = new Message({sender: agentSend.uri, receiver: agentReceives.uri, 
                                      content: {"item": 'Latas', "count": 20, "price": firstPropose.content.price + 1, "period": 1 }, 
                                      performative: 'Propose', conversationId: cpfMessage.conversationId,
                                      inReplyTo: 1234, replyWith: 123456});
        assert.equal(courierInMemory.outbox.length, 0);
        assert.equal(agentReceives.conversations[0].messages.length, 3)
        //unificar le agregar con procesar??
        agentReceives.addMessageToConversation(secondProposeMessage);
        agentReceives.processMessage(secondProposeMessage, courierInMemory);
        assert.equal(agentReceives.conversations[0].messages.length, 5)
        assert.equal(courierInMemory.outbox.length, 1);
        assert.equal(courierInMemory.outbox[0].performative, 'AcceptProposal')
        assert.equal(courierInMemory.outbox[0].inReplyTo, 12345)
      });

      it('should return RejectProposal', function() {
        let secondProposeMessage = new Message({sender: agentSend.uri, receiver: agentReceives.uri, 
                                      content: {"item": 'Latas', "count": 20, "price": firstPropose.content.price - 1, "period": 1 }, 
                                      performative: 'Propose', conversationId: cpfMessage.conversationId,
                                      inReplyTo: 1234, replyWith: 123457});
        assert.equal(courierInMemory.outbox.length, 0);
        assert.equal(agentReceives.conversations[0].messages.length, 3)
        //unificar le agregar con procesar??
        agentReceives.addMessageToConversation(secondProposeMessage);
        agentReceives.processMessage(secondProposeMessage, courierInMemory);
        assert.equal(agentReceives.conversations[0].messages.length, 5)
        assert.equal(courierInMemory.outbox.length, 1);
        assert.equal(courierInMemory.outbox[0].performative, 'RejectProposal')
        assert.equal(courierInMemory.outbox[0].inReplyTo, 123457)      
      });
    })

    describe('#AcceptProposal', function() {
      it('should return failure', function() {
        let acpMessage = new Message({sender: agentSend.uri, receiver: agentReceives.uri, 
                                      content: {"item": 'Latas', "count": 120 }, 
                                      performative: 'AcceptProposal'});
        agentReceives.addMessageToConversation(acpMessage);
        assert.equal(courierInMemory.outbox.length, 0);
        agentReceives.processMessage(acpMessage, courierInMemory);
        assert.equal(courierInMemory.outbox.length, 1);
        assert.equal(courierInMemory.outbox[0].performative, 'Failure')
      });

      it('should return Inform', function() {
        let acpMessage = new Message({sender: agentSend.uri, receiver: agentReceives.uri, 
                                      content: {"item": 'Latas', "count": 20 }, 
                                      performative: 'AcceptProposal'});
        agentReceives.addMessageToConversation(acpMessage);
        assert.equal(courierInMemory.outbox.length, 0);
        agentReceives.processMessage(acpMessage, courierInMemory);
        assert.equal(courierInMemory.outbox.length, 1);
        assert.equal(courierInMemory.outbox[0].performative, 'Inform')
      });
    })

    describe('#RejectProposal', function() {
      it('should return Propose', function() {
        let rcpMessage = new Message({sender: agentSend.uri, receiver: agentReceives.uri, 
                                      content: {"item": 'Latas', "count": 20 }, 
                                      performative: 'RejectProposal'});
        agentReceives.addMessageToConversation(rcpMessage);
        assert.equal(courierInMemory.outbox.length, 0);
        agentReceives.processMessage(rcpMessage, courierInMemory);
        assert.equal(courierInMemory.outbox.length, 1);
        assert.equal(courierInMemory.outbox[0].performative, 'Propose')
        assert.equal(courierInMemory.outbox[0].content.price, 12);
      });
    })
    
    describe('#Inform', function() {
      beforeEach(function() {
        // Conversacion iniciada
        cpfMessage = new Message({sender: agentSend.uri, receiver: agentReceives.uri, 
                                      content: {"item": 'Latas', "count": 20 }, 
                                      performative: 'CallForProposal', replyWith: 1234});
        agentSend.addMessageToConversation(cpfMessage);
        firstPropose = new Message({sender: agentReceives.uri, receiver: agentSend.uri, 
                                      content: {"item": 'Latas', "count": 20, "price": 12, "period": 1 }, 
                                      performative: 'Propose', conversationId: cpfMessage.conversationId,
                                      inReplyTo: 1234, replyWith: 12345});
        acceptPropose =  new Message({sender: agentSend.uri, receiver: agentReceives.uri, 
                                      content: {"item": 'Latas', "count": 20, "price": 12, "period": 1 }, 
                                      performative: 'AcceptProposal', conversationId: cpfMessage.conversationId,
                                      inReplyTo: 12345});
        agentSend.addMessageToConversation(firstPropose);
        agentSend.addMessageToConversation(acceptPropose);
        
      });

      it('should return propose', function() {
        let informMessage = new Message({sender: agentReceives.uri, receiver: agentSend.uri, 
                                      content: {"item": 'Latas', "count": 20 }, 
                                      performative: 'Inform', conversationId: cpfMessage.conversationId});
        agentSend.addMessageToConversation(informMessage);
        assert.equal(courierInMemory.outbox.length, 0);
        agentSend.processMessage(informMessage, courierInMemory);
        assert.equal(courierInMemory.outbox.length, 1);
        assert.equal(courierInMemory.outbox[0].performative, 'Propose');
      });
    })

    // describe('#Failure', function() {
    //   it('should not return any message', function() {
    //     let cancelMessage = new Message({sender: agentSend.uri, receiver: agentReceives.uri, 
    //                                   content: {"item": 'Latas', "count": 20 }, 
    //                                   performative: 'Cancel'});
    //     agentReceives.addMessageToConversation(cancelMessage);
    //     assert.equal(courierInMemory.outbox.length, 0);
    //     agentReceives.processMessage(cancelMessage, courierInMemory);
    //     assert.equal(courierInMemory.outbox.length, 0);
    //   });
    // })
  });
});