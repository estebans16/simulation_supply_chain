var assert = require('assert');
var Message = require('../src/message');

describe('Message', function() {
  describe('#Validations', function() {
    beforeEach(function() {
      someSenderUrl = "http://localhost/agents/1";
      someReceiverUrl = "http://localhost/agents/2";
      someContent = {count: 5};
      someConversation = 1
      aMessage = new Message({sender: someSenderUrl, receiver: someReceiverUrl, content: someContent, performative: 'CallForProposal', conversationId: someConversation});
    });

    it('should return valid', function() {
      var message = new Message({sender: someSenderUrl, receiver: someReceiverUrl, content: someContent, performative: 'CallForProposal', conversationId: someConversation});
      assert.equal(message.isValid(), true);
    });

    it('should return invalid, undefined attributes', function() {
      aMessage.content = undefined;
      assert.equal(aMessage.isValid(), false);
      assert.equal(aMessage.errors().isEmpty(), false);
      assert.equal(aMessage.errors().fullMessage().toString().includes('missing the parameter content'), true);
    });

    it('should return invalid, empty attributes', function() {
      aMessage.content = "";
      assert.equal(aMessage.isValid(), false);
      assert.equal(aMessage.errors().isEmpty(), false);
      assert.equal(aMessage.errors().fullMessage().includes('the parameter content can not be blank'), true);
    });

    it('should return invalid, wrong message type', function() {
      aMessage.performative = 'Tipo de mensaje incorrecto';
      assert.equal(aMessage.isValid(), false);
      assert.equal(aMessage.errors().isEmpty(), false);
      assert.equal(aMessage.errors().fullMessage().includes('the parameter performative wrong value'), true);
    });
  });
});