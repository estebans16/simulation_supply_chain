var Mocks = require('../test/mocks/mocks');
var assert = require('assert');
var PersistDynamoDb = require('../src/persistDynamoDb');
var ItemStock = require('../src/itemStock');
var Agent = require('../src/agent');
var Chain = require('../src/chain');


// TODO
describe('PersistDynamoDb', function() {
  describe('#Agent', function() {
    beforeEach(function() {
      new Mocks().agentMocks();
      let providers = [1, 2, 3];
      let stock = new ItemStock({item: "etiquetas", price: 15, count: 20, reserved: 0, availableAtDate: new Date()});
      let stockEnvases = new ItemStock({item: "envases", price: 15, count: 20, reserved: 0, availableAtDate: new Date()});
      let conversations = [];
      agent = new Agent({agentName: "Fabrica de etiquetas", providers: providers, stock: stock, 
                         conversations: conversations, active: true , transactionNumber: null, 
                         id: null, chainId: 1, uri: null, is_external: false });
      agent2 = new Agent({agentName: "Fabrica de etiquetas2", providers: providers, stock: stock, 
                         conversations: conversations, active: true , transactionNumber: 1, 
                         id: 2, chainId: 1, uri: 'http://localhost/agents/2', is_external: false });
      persistDynamoDb = new PersistDynamoDb();
    });
    afterEach(function() { new Mocks().awsRestore(); });
    
    it('should save, create', function() {
      return persistDynamoDb.saveAgent(agent)
      .then(data => {
        assert.equal(data, true);
      });
    });

    it('should save, update', function() {
      agent2.active = false;
      return persistDynamoDb.saveAgent(agent2)
      .then(data => {
        let dataUpdated = data.Attributes; 
        assert.equal(dataUpdated.chainId, agent2.chainId);
        assert.equal(dataUpdated.uri, agent2.uri);
        assert.equal(dataUpdated.active, false);
      });
    });

    it('should find', function() {
      let agentId = 2;
      return persistDynamoDb.findAgent(agentId)
      .then(data => {
        assert.equal(data.id, agentId);
        assert.equal(data.agentName, 'Fabrica de etiquetas2');
        assert.equal(data.uri, agent2.uri);
      })
    });

  //   it('should not save', function() {
    
  //   });

  //   it('should find', function() {
    
  //   });

  //   it('should not find', function() {
   
  //   });

  //   it('should get all', function() {
    
  //   });

  //   it('should get any', function() {
  //    });
  });

  describe('#Agent -> allAgents', function() {
    beforeEach(function() {
      new Mocks().allAgentsMocks();
      persistDynamoDb = new PersistDynamoDb();
    });
    afterEach(function() { new Mocks().awsRestore(); });

    it('should get all', function() {
      return persistDynamoDb.allAgents()
      .then(data => {
        assert.equal(data instanceof Array, true);
        assert.equal(data.length, 1);
      })
    });

  //   it('should get any', function() {
  //   });
  });

  describe('#Chain', function() {
    beforeEach(function() {
      new Mocks().chainMocks();
      chain = new Chain({chainName: 'Cadena de test2', template: true});
      chain2 = new Chain({chainName: 'Cadena de prueba 2', template: false, transactionNumber: 1});
      persistDynamoDb = new PersistDynamoDb();
    });
    afterEach(function() { new Mocks().awsRestore(); });

    it('should save, create chain', function() {
      return persistDynamoDb.saveChain(chain)
      .then(data => {
        assert.equal(data, true);
      });
    });

    it('should save, update chain', function() {
      return persistDynamoDb.saveChain(chain2)
      .then(data => {
        assert.equal(data, true);
      });
    });

    it('should find', function() {
      let chainId = 7881107934117958;
      return persistDynamoDb.findChain(chainId)
      .then(data => {
        assert.equal(data.id, chainId);
        assert.equal(data.chainName, 'Chain');
      })
    });

  //   it('should not find', function() {
  //   });
  });
  describe('#Chain -> allChains', function() {
    beforeEach(function() {
      new Mocks().allChainsMocks();
      persistDynamoDb = new PersistDynamoDb();
    });
    afterEach(function() { new Mocks().awsRestore(); });

    it('should get all', function() {
      return persistDynamoDb.allChains()
      .then(data => {
        assert.equal(data instanceof Array, true);
        assert.equal(data.length, 1);
      })
    });

  //   it('should get any', function() {
  //   });
  });
});