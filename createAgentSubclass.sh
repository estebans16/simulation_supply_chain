#!/bin/bash

echo "Enter the name of the subclass of Agent to create (e.g. AgentSubclass): "

read file

if [ -z "$file" ]; then
    file="AgentSubclass"
fi

mkdir -p  src/subclasses_agent

echo "Create the file src/subclasses_agent/$file.js"

cp "src/template/template.js" "src/subclasses_agent/$file.js"

sed -i -e "s/Template/$file/g" "src/subclasses_agent/$file.js"