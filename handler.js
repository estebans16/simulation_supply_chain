'use strict';
var Message = require('./src/message');
var CallForProposal = require('./src/message')['CallForProposal'];
let Propose = require('./src/message')['Propose'];
let AcceptProposal = require('./src/message')['AcceptProposal'];
let RejectProposal = require('./src/message')['RejectProposal'];
let InformDone = require('./src/message')['InformDone'];
let InformResult = require('./src/message')['InformResult'];
let Failure = require('./src/message')['Failure'];
let Refuse = require('./src/message')['Refuse'];
var Conversation = require('./src/conversation');
var Chain = require('./src/chain');
let Agent = require('require-all')(__dirname + '/src/subclasses_agent');
let Configuration = require('./src/config/configuration');
let db = new Configuration().db;

// Para levantar servidor local realizar lo siguiente en una consola
// serverless offline
const responseHeaders = {
  'Content-Type':'application/json',
  'Access-Control-Allow-Origin' : '*',        // Required for CORS support to work
  'Access-Control-Allow-Credentials' : true   // Required for cookies, authorization headers with HTTPS 
}

const responses = {
  success: (data={}, code=200) => {
    return {
      'statusCode': code,
      'headers': responseHeaders,
      'body': JSON.stringify(data)
    }
  },
  error: (error, code=500) => {
    return {
      'statusCode': code,
      'headers': responseHeaders,
      'body': JSON.stringify(error)
    }
  }
}
const errors = {};

function isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}


function loadDataLocal(argument) {
  var fs = require('fs');
  var obj = JSON.parse(fs.readFileSync('src/seed/chains.json', 'utf8'));
  for (let key in obj){
      let chain = obj[key];
      db.saveChain(new Chain(chain));
  }

  var obj = JSON.parse(fs.readFileSync('src/seed/agents.json', 'utf8'));
  for (let key in obj){
      let ag = obj[key];
      let agent = new Agent[ag.agentType](ag);
      db.saveAgent(agent);
  }
}

module.exports.loadData = (event, context, callback) => {
  loadDataLocal();
  callback(null, responses.success('Datos cargados '));
}
// serverless invoke local --function start_agen

module.exports.createAgent = (event, context, callback) => {
  // let requestBody = event.body;
  let requestBody = JSON.parse(event.body);
  let klass = requestBody.agentType
  let agent = new Agent[klass](requestBody);
  if (agent.isValid()){
    db.saveAgent(agent)
    .then(data => callback(null, responses.success('agent created, ' + agent.id, 201)))
    .catch(data => callback(null, responses.error(['internal server error'], 500)));
  }else{
    callback(null, responses.error(agent.errors().fullMessage().split(','), 400)); 
  }
};

module.exports.showAgent = (event, context, callback) => {
  db.findAgent(decodeURI(event.pathParameters.id))
  .then(data => callback(null, responses.success(data.toJson())))
  .catch(data => callback(null, responses.error(['agent not found'], 404 ))) ;
};

module.exports.updateAgent = (event, context, callback) => {
  // let requestBody = event.body;
  var requestBody = JSON.parse(event.body);
  db.findAgent(decodeURI(event.pathParameters.id))
  .then(agent =>{ 
                  agent.updateAttributes(requestBody);
                  if (agent.isValid()){
                    db.saveAgent(agent)
                    .then(data => callback(null, responses.success('agent updated', 200)))
                    .catch(data => callback(null, responses.error(['internal server error'], 500)));
                  }else{
                    callback(null, responses.error(agent.errors().fullMessage().split(','), 400));
                  }
                } 
  )
  .catch(data => callback(null, responses.error(['agent not found'], 404)));
};

module.exports.createDatabase =  (event, context, callback) => {
  db.createTables()
  .then(data => callback(null, responses.success('Base de datos creada ' + data)))
  .catch(data => callback(null, responses.success(['Error crear la base de datos '] + data)));
}

module.exports.agents =  (event, context, callback) => {
  db.allAgents()
  .then(data => callback(null, responses.success(data.map(agent => agent.toJson() ))))
  .catch(data => callback(null, responses.error(['internal server error'],500)));
}

module.exports.createMessage =  (event, context, callback) => {
  var requestBody = JSON.parse(event.body);
  // var requestBody = event.body;
  let message = new Message[requestBody.performative](requestBody);
  if (message.isValid()){
    db.findAgent(decodeURI(event.pathParameters.id))
    .then(agent => { 
      try {
        if (agent.addMessageToConversation(message)){
          agent.processMessage(message);
          db.saveAgent(agent)
          .then(data => { 
            agent.courier.deliverMessages()
            .then(data => callback(null, responses.success('added message, conversation: ' + message.conversationId, 201)))
            .catch(data => responses.error(['execution error'], 500))
          })
          .catch(data => { 
            // Agente no se pudo guardar
            if (data.code == 'ConditionalCheckFailedException') {
              agent.courier.cleanQueue();
              agent.courier.queue(message);
              agent.courier.deliverMessages();
              callback(null, responses.error(['agent not save, forwarding the message'], 400) );
            }else {
              callback(null, responses.error(['agent can not process the message'], 403) );
            }
          })
        }else{
          callback(null, responses.error(['agent can not process the message'], 403));
        }
      }catch(error) {
        console.error(error); 
        callback(null, responses.error(['agent can not process the message'], 403));
      }
    })
    .catch(data => callback(null, responses.error(['agent not found'], 404)));
    
  }else{
    callback(null, responses.error(message.errors().fullMessage().split(','), 400));
  }
}

// export SLS_DEBUG=true

module.exports.chains =  (event, context, callback) => {
  db.allChains()
  .then(data => callback(null, responses.success(data, 200)))
  .catch(data => callback(null, responses.error(['internal server error'], 500)));
}

module.exports.createChain =  (event, context, callback) => {
  // var requestBody = event.body;
  var requestBody = JSON.parse(event.body);
  let chain = new Chain(requestBody);
  if (chain.isValid()){
    db.saveChain(chain)
    .then(data => callback(null, responses.success('chain created ' + chain.id, 201)))
    .catch(data => callback(null, responses.error(['internal server error'], 500)));
  }else{
    callback(null, responses.error(chain.errors().fullMessage().split(','), 400));
  }
}

module.exports.updateChain =  (event, context, callback) => {
  // var requestBody = event.body;
  var requestBody = JSON.parse(event.body);
  db.findChain(decodeURI(event.pathParameters.id))
  .then(chain => { chain.updateAttributes(requestBody);
                    if (chain.isValid()){
                      db.saveChain(chain)
                      .then(data => callback(null, responses.success('chain updated ' + chain.id, 201)))
                      .catch(data => callback(null, responses.error(['internal server error'], 500)));
                    }else{
                      callback(null, responses.error(chain.errors().fullMessage().split(','), 400));
                    }               
                 }
  )
  .catch(data => callback(null, responses.error(['internal server error'], 500)));
}

module.exports.showChain =  (event, context, callback) => {
  db.findChain(decodeURI(event.pathParameters.id))
  .then(data => callback(null, responses.success(data.toJson(), 200)))
  .catch(data => callback(null, responses.error(['chain not found'], 404)));
}

module.exports.cloneChain =  (event, context, callback) => {
  db.findChain(decodeURI(event.pathParameters.id))
  .then(chain => {  let originalChain = new Chain(chain);
                    let dupChain = originalChain.clone();
                    db.saveChain(dupChain)
                    .then(data =>  callback(null, responses.success('chain clone ' + dupChain.id, 201)))
                    .catch(data => callback(null, responses.error(['internal server error'], 500)))
                  }
  )
  .catch(data => callback(null, responses.error(['chain not found'], 404)));
}

module.exports.clearChain = (event, context, callback) => {
  db.findChain(decodeURI(event.pathParameters.id))
  .then(chain => {  chain.clearConversations();
                    db.saveChain(chain)
                    .then(data =>  callback(null, responses.success('chain conversations clear ', 201)))
                    .catch(data => callback(null, responses.error(['internal server error'], 500)))
                  }
  )
  .catch(data => callback(null, responses.error(['chain not found'], 404)));
}

module.exports.agentsOfChain =  (event, context, callback) => {
  db.agentsOfChain(decodeURI(event.pathParameters.id))
  .then(data => callback(null, responses.success(data.map(agent =>  agent.toJson()))))
  .catch(data => callback(null, responses.error(['internal server error'], 500)));
}

module.exports.subclassesOfAgents = (event, context, callback) => {
  let agentSubClasses = Object.keys(Agent);
  let response = agentSubClasses.map(function(subClass) {
    let klass = new Agent[subClass]({});
    let comment = klass.comment();
    return  { class: subClass, storeAttribute: klass.defaultStore(), comment };
  }); 
  callback(null, responses.success(response, 200))
}

module.exports.conversationsOfChain = (event, context, callback) => {
  db.conversationsOfChain(decodeURI(event.pathParameters.id))
  .then(conversations => {
    // Me quedo con los distintos id de conversacion
    let conversationsIds = conversations.map(conversation => conversation.id).filter((x, i, a) => a.indexOf(x) == i);
    callback(null, responses.success(conversationsIds))
  })
  .catch(data => callback(null, responses.error(['internal server error'], 500)));
}

module.exports.conversationOfChain = (event, context, callback) => {
  db.conversationsOfChain(decodeURI(event.pathParameters.id))
  .then(conversations => {
    // Seleccionar todas las conversaciones con el id de conversacion recibido
    // armar la conversacion sin mensajes repetidos
    let conversationInAgents = conversations.filter(conver => conver.id == event.pathParameters.conversation_id);
    // todos los mensajes en un mismo nivel
    let allMessages = conversationInAgents.map(agentConv => agentConv.messages).reduce((acc, val) => acc.concat(val));
    callback(null, responses.success(createConversation(allMessages)));
  })
  .catch(data => callback(null, responses.error(['internal server error'], 500)));
}
// TODO
module.exports.reportConversationOfChain = (event, context, callback) => {
  // callback(null, responses.success('TODO'));
  db.conversationsOfChain(decodeURI(event.pathParameters.id))
  .then(conversations => {
    let conversationInAgents = conversations.filter(conver => conver.id == event.pathParameters.conversation_id);
    // todos los mensajes en un mismo nivel
    let allMessages = conversationInAgents.map(agentConv => agentConv.messages).reduce((acc, val) => acc.concat(val));
    let report = createReport(createConversation(allMessages));
    callback(null, responses.success(report));
  })
  .catch(data => callback(null, responses.error(['internal server error'], 500)));
}

module.exports.destroyAgent = (event, context, callback) => {
  db.destroy(decodeURI(event.pathParameters.id), 'agents')
  .then(data => {
    callback(null, responses.success('agent detroy'));
  })
  .catch(data => callback(null, responses.error(['internal server error'], 500)));
}

module.exports.destroyChain = (event, context, callback) => {
  db.agentsOfChain(decodeURI(event.pathParameters.id))
  .then(data => {
    data.every(agent => db.destroy(agent.id, 'agents'));
    db.destroy(decodeURI(event.pathParameters.id), 'chains')
    .then(data => {
      callback(null, responses.success('chain detroy'));
    })
    // .catch(data => callback(null, responses.error(['internal server error'], 500)));
  })
  // .catch(data => callback(null, responses.error(['internal server error'], 500)));
}


function createConversation(messages) {
  let res = [];
  // quitar mensajes repetidos
  messages.forEach(function(element) {
    if (!res.some(ele => (element.performative == ele.performative && element.replyWith == ele.replyWith && element.sender == ele.sender))){
      res.push(element); 
    }
  });
  // los ordeno en el tiempo
  let messagesOfConversationOrder = res.sort((a,b) => compareMessages(a, b));
  return messagesOfConversationOrder;
}

function createReport(conversation) {
  let jsonResponse = { numberOfMessages:{ key: "Messages Count", value: conversation.length } }
  jsonResponse["startAtSimulation"] = { key: "Start of the simulation to", value: conversation[0].createdAt  };
  jsonResponse["endAtSimulation"] = { key: "End of the simulation to", value: conversation[conversation.length - 1].createdAt };
  let messageInformDone = conversation.filter( message => message.performative == 'InformDone');
  jsonResponse["totalNegotiated"] =  { key: "Total negotiated", value: negotiated(messageInformDone)};
  jsonResponse["messageType"] = { key: "Message Type", value: null };
  let messageTypes = conversation.map(message => message.performative);
  let messageOcurrence = occurrence(messageTypes);
  for(var occ in messageOcurrence) {
    jsonResponse[occ] = { key: occ, value: messageOcurrence[occ].length };
  };
  return jsonResponse;
}

function compareMessages(a, b) {
  if (a.createdAt < b.createdAt) {
    return -1;
  }
  if (a.createdAt > b.createdAt) {
    return 1;
  }
  // a debe ser igual b
  return 0;
}

function occurrence (array) {
  "use strict";
  let result = {};
  if (array instanceof Array) { // Check if input is array.
      array.forEach(function (v, i) {
          if (!result[v]) { // Initial object property creation.
              result[v] = [i]; // Create an array for that property.
          } else { // Same occurrences found.
              result[v].push(i); // Fill the array.
          }
      });
  }
  return result;
};

function negotiated(messages) {
  const add = (a, b) =>
    a + b
  let prices = messages.map(message => message.content.count * message.content.price);
  return prices.reduce(add);
}