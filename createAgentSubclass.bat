@echo off

SET /P file= Enter the name of the subclass of Agent to create (e.g. AgentSubclass): 

IF "%file%"=="" (SET file=AgentSubclass)

IF NOT EXIST src\subclasses_agent (MKDIR src\subclasses_agent)

echo Create the file src\subclasses_agent\%file%.js

COPY src\template\template.js src\subclasses_agent\%file%.js>nul

setlocal enableextensions disabledelayedexpansion

set "search=Template"
set "replace=%file%"

set "textFile=src\subclasses_agent\%file%.js"

for /f "delims=" %%i in ('type "%textFile%" ^& break ^> "%textFile%" ') do (
    set "line=%%i"
    setlocal enabledelayedexpansion
    >>"%textFile%" echo(!line:%search%=%replace%!
    endlocal
)