"use strict";
var Validator = require('./validator');

class Chain{
  constructor(chain={}){
    this.id = chain.id || this.generateRandomId();
    this.chainName = chain.chainName;
    this.description = chain.description || ' ';
    this.template = chain.template || false;
    this.transactionNumber = chain.transactionNumber || 0;
    this.agents = chain.agents || [];
  }

  validator(){
    if (this.avalidator instanceof Validator){
      return this.avalidator;
    }
    this.avalidator = new Validator(this);
    return this.avalidator;
  }

  isValid(){
    this.validator().clearErrors();
    this.validator().present(this.attributesToValidate());
    this.validator().blank(this.attributesToValidate().filter(atr => atr !== 'template'));
    if (this.agents.length != 0){
      let agentsErrors = this.agents.map(agent => {
        if (!agent.isValid()){
          return agent.errors().fullMessage();
        }
        return null;
      }).filter(function (el) { return el != null; });
      if (agentsErrors.length != 0){
        this.errors().add('agents', agentsErrors);  
      }
    }
    return this.validator().errors.isEmpty();
  }

  errors(){
    return this.validator().errors;    
  }

  attributes(){
    return ["id", "chainName", "template", "description", "transactionNumber"];
  }

  attributesToValidate(){
    return ["id", "chainName", "template"];
  }

  toJson(){
    var json = {
      "id": this.id,
      "chainName": this.chainName,
      "agents": this.agents,
      "description": this.description,
      "template": this.template,
      "transactionNumber": this.transactionNumber
    }
    return json;
  }

  generateRandomId(){
    var EPOCH = 1300000000000;
    var ts = new Date().getTime() - EPOCH; // limit to recent
    var randid = Math.floor(Math.random() * 512);
    ts = (ts * 64);   // bit-shift << 6
    return (ts * 512) + (randid % 512);
  }

  updateAttributes(params={}){
    for (var key in params){
      if (params[key] != undefined){
        this[key] = params[key];
      };
    }
  }

  // static find(id){
  //   let promise = new Promise((resolve, reject) => {
  //     DatabaseHandling.find(id, 'chains')
  //     .then(chain => {
  //       DatabaseHandling.agentsOfChain(chain.id)
  //       .then(data => {
  //         let agents = data.map(agent => new Agent(agent));
  //         chain['agents'] = agents;
  //         resolve(new Chain(chain));
  //       })
  //       .catch(data => reject(data));
  //     })
  //     .catch(data => reject(data));
  //   });
  //   return promise;
  // }

  isPersisted(){
    return !(this.transactionNumber == 0);
  }

  // save(){
  //   let promise = new Promise((resolve, reject) => {
  //     if (this.isValid()){
  //       this.saveAgents()
  //       .then(data => {
  //         if (data){
  //           this.saveChain()
  //           .then(data => resolve(true))
  //           .catch(error => resolve(false))
  //         }
  //       })
  //       .catch(error => resolve(false))
  //     }else{
  //       resolve(false)
  //     }
  //   });
  //   return promise;
  // }

  // saveChain(){
  //   let promise = new Promise((resolve, reject) => {
  //     if (this.isPersisted()) {
  //       this.transactionNumber += 1;
  //       DatabaseHandling.update(this)
  //       .then(data => resolve(data))
  //       // .catch(error => resolve(error));
  //     }else{
  //       this.transactionNumber = 1;
  //       DatabaseHandling.create(this)
  //       .then(data => resolve(true))
  //       .catch(error => {
  //         this.errors().add('database', data);
  //         resolve(error);
  //       });
  //     }
  //   })
  //   return promise;
  // }

  // saveAgents(){
  //   let promise =  new Promise((resolve, reject) => {
  //     if (this.agents.length == 0){
  //       resolve(true);
  //     }else{
  //       let requests = this.agents.map(agent => {agent.save()});
  //       Promise.all(requests).then(responses => { 
  //         resolve(!responses.includes('false'));
  //       });
  //     }
  //   });
  //   return promise;
  // }

  clone(){
    let clone = new Chain({chainName: this.chainName + ' copy',
                          description: this.description});
    let cloneAgent = null;
    let saved = false;
    this.agents.forEach(agent => {
      cloneAgent = agent.clone();
      cloneAgent.chainId = clone.id;

      clone.agents.push(cloneAgent);
    });

    return clone;
    /*Esto se hace en el metodo save
    let promise =  new Promise((resolve, reject) => {
      let requests = clone.agents.map(agent => { agent.save() });

      Promise.all(requests).then(responses => { 
        resolve(!responses.include(false));
      });
    })
    
    return promise;
    */
  }

  clearConversations(){
    this.agents.forEach(agent => {
      agent.conversations = [];
    });
  }
}

module.exports = Chain;