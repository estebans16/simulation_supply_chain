'use strict';
var Validator = require('./validator');
class Message {
  // http://www.fipa.org/specs/fipa00061/SC00061G.html#_Toc26669710 Docs FIPA messages
  constructor(params={}) {
    this.sender = params.sender;
    this.receiver = params.receiver;
    this.content = params.content;
    this.conversationId = params.conversationId;
    this.replyWith = params.replyWith;
    this.inReplyTo = params.inReplyTo;
    this.performative = params.performative;
    this.id = params.id || generateRandomId();
    this.createdAt = params.createdAt || new Date().toISOString();
  }

  validator(){
    if (this.avalidator instanceof Validator){
      return this.avalidator;
    }
    this.avalidator = new Validator(this);
    return this.avalidator;
  }

  isValid(){
    this.validator().clearErrors();
    var typesOfMessages = ["CallForProposal", "Propose", "AcceptProposal", "RejectProposal", 
                           "InformDone", "InformResult", "Failure", "Refuse"];
    this.validator().present(this.attributesToValidate());
    this.validator().blank(this.attributesToValidate());
    this.validator().posibleValue('performative', typesOfMessages);
    return this.validator().errors.isEmpty();
  }

  errors(){
    return this.validator().errors;    
  }

  attributes(){
    return ["sender", "receiver", "content", "performative", "conversationId", "replyWith", "inReplyTo", "createdAt"];
  }

  attributesToValidate(){
    return ["sender", "receiver", "content", "performative"];
  }

  toJson(){
    var a = { 
      "sender": this.sender,
      "receiver": this.receiver,
      "content": this.content,
      "performative": this.performative,
      "conversationId": this.conversationId,
      "replyWith": this.replyWith,
      "inReplyTo": this.inReplyTo,
      "createdAt": this.createdAt,
      "id": this.id
    };
    return a;
  }

  asFailureTemplate(){
    let message = new Failure({ sender: this.receiver, receiver: this.sender,
                                conversationId: this.conversationId,
                                inReplyTo: this.replyWith, content: this.content})
    return message;
  }
}

class CallForProposal extends Message {
  constructor(params){
    super(params);
    this.performative = 'CallForProposal';
  }
  asProposeTemplate(){
    let message = new Propose({ sender: this.receiver, receiver: this.sender,
                                conversationId: this.conversationId,
                                inReplyTo: this.replyWith, replyWith: generateRandomId()})
    return message;
  }

  asRefuseTemplate(){
    let message = new Refuse({ sender: this.receiver, receiver: this.sender,
                                conversationId: this.conversationId,
                                inReplyTo: this.replyWith, content: this.content})
    return message;
  }
}


class Propose extends Message {

  constructor(params){
    super(params);
    this.performative = 'Propose';
  }

  asAcceptProposalTemplate(){
    let message = new AcceptProposal({ sender: this.receiver, receiver: this.sender,
                                       conversationId: this.conversationId,
                                       inReplyTo: this.replyWith, content: this.content,
                                       replyWith: generateRandomId() })
    return message;
  }

  asRejectProposalTemplate(){
    let message = new RejectProposal({ sender: this.receiver, receiver: this.sender,
                                       conversationId: this.conversationId,
                                       inReplyTo: this.replyWith, content: this.content})
    return message;
  }
}

class AcceptProposal extends Message {
  constructor(params){
    super(params);
    this.performative = 'AcceptProposal';
  }

  asInformDoneTemplate(){
    let message = new InformDone({ sender: this.receiver, receiver: this.sender,
                                   conversationId: this.conversationId,
                                   inReplyTo: this.replyWith, content: this.content,
                                   replyWith: generateRandomId()})
    return message;
  }

  asInformResultTemplate(){
    let message = new InformResult({ sender: this.receiver, receiver: this.sender,
                                     conversationId: this.conversationId, content: this.content,
                                     inReplyTo: this.replyWith})
    return message;
  }
}

class RejectProposal extends Message {
  constructor(params){
    super(params);
    this.performative = 'RejectProposal';
  }

  asProposeTemplate(){
    let message = new Propose({ sender: this.receiver, receiver: this.sender,
                                conversationId: this.conversationId, 
                                inReplyTo: this.replyWith, replyWith: generateRandomId()})
    return message;
  }
}

class InformDone extends Message {
  constructor(params){
    super(params);
    this.performative = 'InformDone';
  }
}

class InformResult extends Message {
  constructor(params){
    super(params);
    this.performative = 'InformResult';
  }
}

class Failure extends Message {
  constructor(params){
    super(params);
    this.performative = 'Failure';
  }
}

class Refuse extends Message {
  constructor(params){
    super(params);
    this.performative = 'Refuse';
  }
}


function generateRandomId () {
  let EPOCH = 1300000000000;
  let ts = new Date().getTime() - EPOCH; // limit to recent
  let randid = Math.floor(Math.random() * 512);
  ts = (ts * 64);   // bit-shift << 6
  return (ts * 512) + (randid % 512);
};


module.exports = { Message, CallForProposal, Propose, AcceptProposal, RejectProposal,
                   InformDone, InformResult, Failure, Refuse };