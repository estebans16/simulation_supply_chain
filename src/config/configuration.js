var PersistDynamoDb = require('../persistDynamoDb');
var PersistLocalDynamoDb = require('../persistLocalDynamoDb');

class Configuration {
  constructor() {
  	// CAMBIAR POR VARIABLE DE ENTORNO
  	this.readYml();
  	if (this.configYml.ENVIRONMENT == 'local'){
	    this.db = new PersistLocalDynamoDb();
      this.defaultUri = 'http://localhost:3000';
      this.db.tableExist('agents')
      .catch(data => {this.db.createTables()
                      .then(data => console.log('tablas creadas'))
                      .catch(data => console.log(''))
        
      })
  	}else{
			this.db = new PersistDynamoDb();		
      this.defaultUri = 'https://wcch7nt8dg.execute-api.us-east-1.amazonaws.com/dev';
  	}
  }

  readYml(){
  	this.configYml = require('./config.json');
  }

}

module.exports = Configuration;