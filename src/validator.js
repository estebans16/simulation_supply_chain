'use strict';
var Errors = require('./errors');
class Validator {
  constructor(obj) {
    this.objectToValidate = obj;
    this.errors = new Errors();
  }

  present(attributes){
    for (var key in attributes){
      var attribute = attributes[key];
      if (this.objectToValidate[attribute] == undefined ) {
        this.errors.add(attribute, 'missing the parameter ' + attribute);
      }
    }
    return this.errors;
  }

  blank(attributes){
    for (var key in attributes){
      var attribute = attributes[key];
      if (this.isBlank(this.objectToValidate[attribute])) {
        this.errors.add(attribute, 'the parameter ' + attribute + ' can not be blank');
      }
    }
    return this.errors;
  }


  isBlank(value){
    return (!value || /^\s*$/.test(value));
  }

  clearErrors(){
    this.errors.clear();
  }

  posibleValue(attribute, values){
    if (!values.includes(this.objectToValidate[attribute])){
      this.errors.add(attribute, 'the parameter ' + attribute + ' wrong value');
    }
  }
};

module.exports = Validator;