'use strict';
var PersistDynamoDb = require('./persistDynamoDb');
class PersistLocalDynamoDb extends PersistDynamoDb {
	constructor(){
		super();
		this.aws = require('aws-sdk');
	  this.docClient = new this.aws.DynamoDB.DocumentClient({region: 'localhost', endpoint: 'http://localhost:8000'});
	  this.ddb =  new this.aws.DynamoDB({apiVersion: '2012-10-08', region: 'localhost', 
	                                       endpoint: 'http://localhost:8000'});

	}
}


module.exports = PersistLocalDynamoDb;