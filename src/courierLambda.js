"use strict";

var Courier = require('./courier');

class CourierLambda extends Courier {
	constructor(){
		super();
	}

    /**
     * Deliver the messages
     */
    deliverMessages() {
        let request = require('request'); 

        //obtengo el primer mensaje encolado
        let promise = new Promise((resolve, reject) => {
            let message = this.outbox.shift();

            // mientras tenga mensajes, los mando
            while (message != undefined) {
                // console.log("sendMessage");
                request.post(
                    message.receiver + '/messages',
                    { json: message.toJson() },
                    function (error, response, body) {
                        if (error){
                            console.log('el mensaje no se envio');
                        }
                    }
                );

                //obtengo el siguiente mensaje
                message = this.outbox.shift();
            }
            resolve(true);
        });
        return promise;
    }
}


module.exports = CourierLambda;