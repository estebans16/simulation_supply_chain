'use strict';
var Agent = require('../agent');
let Message = require('../message');

class Granja extends Agent {

  constructor(dataAgent={}){
    dataAgent.agentType = 'Granja';
    super(dataAgent);
  }
  // Subclasses should redefine this method
  defaultStore(){
    return {
      "products": {
        "Tomates": {"price": 10, "count": 200},
        "Zanahorias": {"price": 1, "count": 100},
      }
    }
  }
  // Subclasses should redefine this method
  processCallForProposal(message){
    if (this.canProvide(message.content.item) && this.canSupplyRequest(message)){
      // armo propuesta
      let price = this.getProduct(message.content.item).price;
      let content = { price: price * parseInt(message.content.count), item: message.content.item, count: message.content.count};
      let propose = message.asProposeTemplate();
      propose.content = content;
      this.queue(propose);
    }else{
      // Refuse
      let refuse = message.asRefuseTemplate();
      refuse.content = message.content;
      this.queue(refuse)
    }
  }
  // Subclasses should redefine this method
  processPropose(message){
  }
  // Subclasses should redefine this method

  processAcceptProposal(message){
    if (this.canProvide(message.content.item)){
      if (this.canSupplyRequest(message)){
        // inform-done
        let response = message.asInformDoneTemplate();
        response.content = message.content ;  
        this.queue(response);
      }
    }else{
      // failure
      let response = message.asFailureTemplate();
      response.content = message.content ;
      this.queue(response);
    }
  }
  // Subclasses should redefine this method
  processRejectProposal(message){
    if (this.canProvide(message.content.item)){
      if (this.canSupplyRequest(message)){
        // armo propuesta
        let price = this.getProduct(message.content.item).price;
        let content = { price: price * parseInt(message.content.count), item: message.content.item, count: message.content.count};
        let propose = message.asProposeTemplate();
        propose.content = content;
        this.queue(propose);
      }
    }
  }
  // Subclasses should redefine this method
  processRefuse(message){
  }
  // Subclasses should redefine this method
  processFailure(message){
  }
  // Subclasses should redefine this method
  processInformDone(message){
  }
  // Subclasses should redefine this method
  processInformResult(message){
  }

  canSupplyRequest(message){
    let prod = this.getProduct(message.content['item']);
    return (parseInt(message.content["count"]) <= parseInt(prod.count));
  }

  getProduct(productName){
    return this.getStore().products[productName];
  }
}

module.exports = Granja;