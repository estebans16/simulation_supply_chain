"use strict";
var Agent = require("../agent");
let Message = require("../message");

class ProgrammableAgent extends Agent {
  constructor(dataAgent = {}) {
    dataAgent.agentType = "ProgrammableAgent";
    super(dataAgent);
  }

  defaultStore() {
    return {
      products: {
        Hope: {
          count: 1,
          price: 1
        }
      },
      scripts: {
        processCallForProposal:
          "console.log('CallForProposal received and ignored');",
        processPropose: "console.log('Propose received and ignored');",
        processAcceptProposal:
          "console.log('AcceptProposal received and ignored');",
        processRejectProposal:
          "console.log('RejectProposal received and ignored');",
        processRefuse: "console.log('Refuse received and ignored');",
        processFailure: "console.log('Failure received and ignored');",
        processInformDone: "console.log('InformeDone received and ignored');",
        processInformResult: "console.log('InformResult received and ignored');"
      }
    };
  }

  getMethodFor(methodName) {
    let src =
      "(self, message) => {\n" +
      this.userStore.scripts[methodName] +
      "\n}";
    return eval(src);
  }

  processCallForProposal(message) {
    let mthd = this.getMethodFor("processCallForProposal");
    mthd(this, message);
  }

  processPropose(message, courier) {
    let mthd = this.getMethodFor("processPropose");
    mthd(this, message);
  }

  processAcceptProposal(message, courier) {
    let mthd = this.getMethodFor("processAcceptProposal");
    mthd(this, message);
  }

  processRejectProposal(message, courier) {
    let mthd = this.getMethodFor("processRejectProposal");
    mthd(this, message);
  }

  processRefuse(message, courier) {
    let mthd = this.getMethodFor("processRefuse");
    mthd(this, message);
  }

  processFailure(message, courier) {
    let mthd = this.getMethodFor("processFailure");
    mthd(this, message);
  }

  processInformDone(message, courier) {
    let mthd = this.getMethodFor("processInformDone");
    mthd(this, message);
  }

  processInformResult(message, courier) {
    let mthd = this.getMethodFor("processInformResult");
    mthd(this, message);
  }

  comment() {
    return 'A programmable agent has one script to handle the reception of each ' + 
        'type of message. Be carefull when programming these scripts, as errors are difficult ' +
        'to debug. If you modify the useStore object, changes will be persistent. You can get ' + 
       'the userStore object of this agent by calling getStore(). WARN: if you enable ' +
       'programmable agents in your environment, you risk injection of harmfull code.'
  }
}

module.exports = ProgrammableAgent;
