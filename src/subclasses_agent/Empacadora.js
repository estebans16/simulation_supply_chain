'use strict';
var Agent = require('../agent');
let Message = require('../message');

class Empacadora extends Agent {

  constructor(dataAgent={}){
    dataAgent.agentType = 'Empacadora';
    super(dataAgent);
  }
  // Subclasses should redefine this method
  defaultStore(){
    return {
      "products": {
        "Paquetes de verduras": {"price": 10, "count": 0},
      },
      "bom": {
        "Tomates": 5,
        "Zanahorias": 6,
        "Zapallos": 7,
        "Envases": 1
      }
    }
  }
  // Subclasses should redefine this method
  // canProvide(producName){
  //   return this.getProducts.includes(producName);
  // }
  // Subclasses should redefine this method
  processCallForProposal(message){
    if (this.canProvide(message.content.item)){
      if (this.canSupplyRequest(message)){
        // armo propuesta
        let price = this.getProduct(message.content.item).price;
        let content = { price: price * parseInt(message.content.count), item: message.content.item, count: message.content.count};
        let propose = message.asProposeTemplate();
        propose.content = content;
        this.queue(propose);
      }else{
        // Pedir a mis proveedores
        if (this.haveSuppliers() && this.canBuildProduct() ){
          this.prepareMessageToProviders(message);
        }else{
          // Refuse
          this.prepareRefuse(message);
        }
      }
    }else{
      // Refuse
      this.prepareRefuse(message);
    }
  }
  // Subclasses should redefine this method
  processPropose(message){
    //espero a todos los propose, me quedo con el menor precio de cada producto
    // let conversation = this.getConversation(message.conversationId);
    let cfpSent = this.cfpSent(message.conversationId);
    let proposeReceived = this.proposeReceived(message.conversationId);
    if ( cfpSent.length == proposeReceived.length  ) {
      let bom = this.getBom();
      for (var key in bom){
        let proposeForItem = proposeReceived.filter(propose => propose.content['item'] == key )
        let minPropose = proposeForItem.reduce(function(prev, current) {
          return ( prev.content['price'] < current.content['price'] ) ? prev : current
        });
        let acceptProposal = this.prepareAcceptProposal(minPropose);
        this.queue(acceptProposal);
      }
    }
  }
  // Subclasses should redefine this method
  processAcceptProposal(message){
    // Genero un inform
    if (this.canSupplyRequest(message)){
      // reducir stock
      let product = this.getProduct(message.content.item);
      product.count -= parseInt(message.content.count);
      this.setProduct(message.content.item, product);
      // Armo informeDone
      let informDone = message.asInformDoneTemplate();
      informDone.content = message.content;
      this.queue(informDone);
    }else{
      let failure = message.asFailureTemplate();
      failure.content = message.content;
      this.queue(failure);
    }
  }
  // Subclasses should redefine this method
  processRejectProposal(message){
  }
  // Subclasses should redefine this method
  processRefuse(message){
    if (!this.alredySentRefuse(message.conversationId)){
      let cpfRecived = this.getConversation(message.conversationId).messages[0];
      this.prepareRefuse(cpfRecived);
    }
  }
  // Subclasses should redefine this method
  processFailure(message){
    let cpfRecived = this.getConversation(message.conversationId).messages[0];
    this.prepareRefuse(cpfRecived);
  }
  // Subclasses should redefine this method
  processInformDone(message){
    let informDr = this.informDoneReceived(message.conversationId);
    let acceptPs = this.acceptProposalSent(message.conversationId);
    if (acceptPs.length == informDr.length){
      let cpfRecived = this.getConversation(message.conversationId).messages[0];
      // aumento mi stock

      let product = this.getProduct(cpfRecived.content.item);
      product.count += parseInt(cpfRecived.content.count);
      this.setProduct(cpfRecived.content.item, product);
      // armo propuesta
      let price = this.getProduct(cpfRecived.content.item).price * parseInt(cpfRecived.content.count) ;
      let content = { price: price, item: cpfRecived.content.item, count: cpfRecived.content.count};
      let propose = cpfRecived.asProposeTemplate();
      propose.content = content;
      this.queue(propose);
    }
  }
  // Subclasses should redefine this method
  processInformResult(message){
  }

  canSupplyRequest(message){
    let prod = this.getProduct(message.content['item']);
    return (parseInt(message.content["count"]) <= parseInt(prod.count));
  }

  getProduct(productName){
    return this.getStore().products[productName];
  }

  setProduct(productName, product){
    this.getStore().products[productName] = product;
  }

  getBom(){
    return this.getStore().bom; 
  }

  prepareRefuse(message){
    let refuse = message.asRefuseTemplate();
    refuse.content = message.content;
    this.queue(refuse);
  }

  prepareMessageToProviders(message){
    let bom = this.getBom();
    let currentStock = parseInt(this.getProduct(message.content["item"]).count);
    let count = parseInt(message.content["count"]) - currentStock;
    for (var key in bom){
      // key es el nombre del producto que necesito
      let suppliersUris = this.getSuppliersUri(key);
      for (let prov in suppliersUris){
        let supplierUri = suppliersUris[prov];
        let content = {
          item: key,
          count: bom[key] * count
        }
        let messageProvider = new Message['CallForProposal']({ sender: this.getUri(), receiver: supplierUri, content: content, 
                                                               conversationId: message.conversationId, replyWith: this.generateRandomId() });
        this.queue(messageProvider);
      };
    };
  }

  prepareAcceptProposal(propose){
    let acceptProposal = propose.asAcceptProposalTemplate();
    acceptProposal.content = propose.content;
    return acceptProposal;
  }
  // Cuento la cantidad de cfp que hice
  cfpSent(conversationId){
    let conversation = this.getConversation(conversationId);
    return conversation.getMessages().filter(message => message.performative == "CallForProposal" && 
                                                                    message.sender == this.getUri());
    

  }
  // Cuento la cantidad de propose que recibi
  proposeReceived(conversationId){
    let conversation = this.getConversation(conversationId);
    return  conversation.getMessages().filter(message => message.performative == "Propose" && 
                                                                    message.receiver == this.getUri());
  }

  informDoneReceived(conversationId){
    let conversation = this.getConversation(conversationId);
    return  conversation.getMessages().filter(message => message.performative == "InformDone" && 
                                                         message.receiver == this.getUri());
  }

  acceptProposalSent(conversationId){
    let conversation = this.getConversation(conversationId);
    return  conversation.getMessages().filter(message => message.performative == "AcceptProposal" && 
                                                         message.sender == this.getUri());
  } 

  canBuildProduct(){
    let bomKeys = Object.keys(this.getBom());
    let ok = true;
    for (var key in bomKeys){
      if (this.getSuppliersUri(bomKeys[key]).length == 0){
        ok = false;
        break;
      }
    }
    return ok;
  }

  alredySentRefuse(conversationId){
    let conversation = this.getConversation(conversationId);
    return conversation.getMessages().some(message => message.performative == "Refuse" && 
                                                         message.sender == this.getUri());
    
  }
}

module.exports = Empacadora;