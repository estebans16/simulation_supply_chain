"use strict";
let Agent = require("../agent");
let Validator = require("../SchemaOrgValidator");
let CallForProposal = require("../message")["CallForProposal"];

class StocklessFactory extends Agent {
  constructor(dataAgent = {}) {
    dataAgent.agentType = "StocklessFactory";
    super(dataAgent);
  }

  comment() {
    return "I build one product. For that I need materials.";
  }

  defaultStore() {
    return {
      products: [
        {
          "@context": "https://schema.org/",
          "@type": "Product",
          name: "Product A",
          manufacturingCost: 10,
          profitPercentage: 0.2,
          billOfMaterials: [
            {
              "@type": "Product",
              name: "Product A1",
              quantity: 1
            },
            {
              "@type": "Product",
              name: "Product A2",
              quantity: 2
            }
          ]
        }
      ],
      projects: {}
    };
  }

  getProductByName(name) {
    return this.getStore().products.find(each => {
      return each.name == name;
    });
  }

  canProvide(aProduct) {
    return this.getProductByName(aProduct.name) != null;
  }

  processCallForProposal(message) {
    let demand = message.content;
    if (!new Validator().isValidDemand(demand)) {
      this.queue(message.asFailureTemplate());
    } else {
      let item = demand.itemOffered;
      let productToOffer = this.getProductByName(item.name);
      if (productToOffer) {
        let project = JSON.parse(JSON.stringify(productToOffer)); //copy
        project.conversationId = message.conversationId;
        project.replyWith = message.replyWith;
        project.receiver = message.sender;
        project.cfpCount = 0;
        this.getStore().projects[project.conversationId] = project;
        project.billOfMaterials.forEach(element => {
          this.askSuppliersFor(element, project);
        });
      } else {
        // Refuse
        let refuse = message.asRefuseTemplate();
        this.queue(refuse);
      }
    }
  }

  askSuppliersFor(aProduct, aProject) {
    let suppliers = this.getSuppliersUri(aProduct.name);
    let demand = {
      "@content": "http://schema.org/",
      "@type:": "Demand",
      itemOffered: {
        name: aProduct.name
      },
      eligibleQuantity: {
        unitCode: "C62",
        value: aProduct.quantity
      }
    };
    suppliers.forEach(supplier => {
      aProject.cfpCount = aProject.cfpCount + 1;
      let cfp = new CallForProposal({
        sender: this.getUri(),
        receiver: supplier,
        content: demand,
        conversationId: aProject.conversationId,
        replyWith:this.generateRandomId()
      });
      this.queue(cfp);
    });
  }

  processPropose(message) {
    let project = this.getStore().projects[message.conversationId];
    if (! project) {    
      return this.queue(message.asFailureTemplate());
    } 
    
    let offer = message.content;

    let material = project.billOfMaterials.find(aMaterial => {
      return (
        offer.itemOffered.name == aMaterial.name &&
        offer.eligibleQuantity.value == aMaterial.quantity
      );
    });

    if (! material) {
      return this.queue(message.asFailureTemplate());
    }

    project.cfpCount = project.cfpCount - 1;

    if ( ! material.bestOfferSoFar || (material.bestOfferSoFar < offer.price)) {
      material.bestOfferSoFar = offer;
    }
    
    if (project.cfpCount == 0) {
      this.submitProposalFor(project);
    }
    
  }

  submitProposalFor(project) {
    let materialsCost = 0;
    project.billOfMaterials.forEach(each => {
      materialsCost = materialsCost + each.bestOfferSoFar.price; 
    })
    let price = (materialsCost + project.manufacturingCost) * (1 + project.profitPercentage);
    let cfp = this.getConversation(project.conversationId).messages[0];
    let propose = cfp.asProposeTemplate();
    propose.content = JSON.parse(JSON.stringify(cfp.content)); //clone
    propose.content["@type"] = "Offer";
    propose.content.price = price;
    this.queue(propose);
  }
}

module.exports = StocklessFactory;
