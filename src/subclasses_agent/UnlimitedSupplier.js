"use strict";
var Agent = require("../agent");
let Validator = require("../SchemaOrgValidator");

class UnlimitedSupplier extends Agent {
  constructor(dataAgent = {}) {
    dataAgent.agentType = "UnlimitedSupplier";
    super(dataAgent);
  }

  comment() {
    return "I offer unlimited supplies of various.";
  }

  defaultStore() {
    return {
      products: [
        {
          "@context": "https://schema.org/",
          "@type": "Product",
          name: "Product A1",
          offers: [
            {
              "@type": "Offer",
              price: 123
            }
          ]
        }
      ]
    };
  }

  getProductByName(name) {
    return this.getStore().products.find(each => {return each.name == name});
  }

  canProvide(aProduct) {
    return this.getProductByName(aProduct.name) != null
  }

  processCallForProposal(message) {
    let demand = message.content;
    if (!new Validator().isValidDemand(demand)) {
      this.queue(message.asFailureTemplate());
    } else {
      let item = demand.itemOffered;
      let quantity = parseInt(demand.eligibleQuantity.value);
      let productToOffer = this.getProductByName(item.name);
      if (productToOffer) {
        let unitPrice = productToOffer.offers[0].price;
        let finalPrice = unitPrice * quantity;
        let offer = {
          "@context" : "http://schema.org/",
          "@type" : "Offer",
          itemOffered: demand.itemOffered,
          eligibleQuantity: demand.eligibleQuantity,
          price: finalPrice
        };
        let propose = message.asProposeTemplate();
        propose.content = offer;
        this.queue(propose);
      } else {
        // Refuse
        let refuse = message.asRefuseTemplate();
        this.queue(refuse);
      }
    }
  }
}

module.exports = UnlimitedSupplier;
