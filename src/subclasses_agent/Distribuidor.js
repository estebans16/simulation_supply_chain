'use strict';
var Agent = require('../agent');
let Message = require('../message');

class Distribuidor extends Agent {

  constructor(dataAgent={}){
    dataAgent.agentType = 'Distribuidor';
    super(dataAgent);
  }
  // Subclasses should redefine this method
  defaultStore(){
    return {
      "products": {
        "Paquetes de verduras": {"price": 10, "count": 1},
      }
    }
  }
  // Subclasses should redefine this method
  processCallForProposal(message){
    if (this.canProvide(message.content.item)){
      if (this.canSupplyRequest(message)){
        // armo propuesta
        let price = this.getProduct(message.content.item).price;
        let content = { price: price, item: message.content.item, count: message.content.count};
        let propose = message.asProposeTemplate();
        propose.content = content;
        this.queue(propose);
      }else{
        // Pedir a mis proveedores
        if (this.haveSuppliers()){
          this.prepareMessageToProviders(message);
        }else{
          // Refuse
          this.prepareRefuse(message);
        }
      }
    }else{
      // Refuse
      this.prepareRefuse(message);
    }
  }
  // Subclasses should redefine this method
  processPropose(message){
    let cfpSent = this.cfpSent(message.conversationId);
    let proposeReceived = this.proposeReceived(message.conversationId);
    if ( cfpSent.length == proposeReceived.length  ) {
      let minPropose = proposeReceived.reduce(function(prev, current) {
        return ( prev.content['price'] < current.content['price'] ) ? prev : current
      });
      let acceptProposal = this.prepareAcceptProposal(minPropose);
      this.queue(acceptProposal);
    }
  }
  // Subclasses should redefine this method
  processAcceptProposal(message){
    if (this.canProvide(message.content.item)){
      if (this.canSupplyRequest(message)){
        // reducir stock
        let product = this.getProduct(message.content.item);
        product.count -= parseInt(message.content.count);
        this.setProduct(message.content.item, product);
        // Armo informeDone
        let informDone = message.asInformDoneTemplate();
        informDone.content = message.content;
        this.queue(informDone);
      }
    }else{
      let failure = message.asFailureTemplate();
      failure.content = message.content;
      this.queue(failure);
    }
  }
  // Subclasses should redefine this method
  processRejectProposal(message){
  }
  // Subclasses should redefine this method
  processRefuse(message){
    let cpfRecived = this.getConversation(message.conversationId).messages[0];
    this.prepareRefuse(cpfRecived);
  }
  // Subclasses should redefine this method
  processFailure(message){
    let cpfRecived = this.getConversation(message.conversationId).messages[0];
    this.prepareRefuse(cpfRecived);
  }
  // Subclasses should redefine this method
  processInformDone(message){
    let cpfRecived = this.getConversation(message.conversationId).messages[0];
    // aumento mi stock
    let product = this.getProduct(cpfRecived.content.item);
    product.count += parseInt(message.content.count);
    this.setProduct(cpfRecived.content.item, product);
    //Armo propuesta
    let price = this.getProduct(cpfRecived.content.item).price * parseInt(cpfRecived.content.count) ;
    let content = { price: price, item: cpfRecived.content.item, count: cpfRecived.content.count};
    let propose = cpfRecived.asProposeTemplate();
    propose.content = content;
    this.queue(propose);
  }
  // Subclasses should redefine this method
  processInformResult(message){
  }

  canSupplyRequest(message){
    let prod = this.getProduct(message.content['item']);
    return (parseInt(message.content["count"]) <= parseInt(prod.count));
  }

  prepareMessageToProviders(message){
    let productName = message.content["item"];
    let currentStock = parseInt(this.getProduct(productName).count);
    let count = parseInt(message.content["count"]) - currentStock;
    // Pido a mis proveedores de producto
    let suppliersUris = this.getSuppliersUri(productName);
    for (let prov in suppliersUris){
      let supplierUri = suppliersUris[prov];
      let content = {
        item: productName,
        count: count
      }
      let messageProvider = new Message['CallForProposal']({ sender: this.getUri(), receiver: supplierUri, content: content, 
                                                             conversationId: message.conversationId, replyWith: this.generateRandomId()});
      this.queue(messageProvider);
    };
  }

  prepareRefuse(message){
    let refuse = message.asRefuseTemplate();
    refuse.content = message.content;
    this.queue(refuse);
  }

  prepareAcceptProposal(propose){
    let acceptProposal = propose.asAcceptProposalTemplate();
    acceptProposal.content = propose.content;
    return acceptProposal;
  }

  cfpSent(conversationId){
    let conversation = this.getConversation(conversationId);
    return conversation.getMessages().filter(message => message.performative == "CallForProposal" && 
                                                                    message.sender == this.getUri());
    

  }

  proposeReceived(conversationId){
    let conversation = this.getConversation(conversationId);
    return  conversation.getMessages().filter(message => message.performative == "Propose" && 
                                                                    message.receiver == this.getUri());
  }

  getProduct(productName){
    return this.getStore().products[productName];
  }

  setProduct(productName, product){
    this.getStore().products[productName] = product;
  }


}

module.exports = Distribuidor;