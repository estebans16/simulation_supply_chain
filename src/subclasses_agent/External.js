'use strict';
var Agent = require('../agent');
let Message = require('../message');

class External extends Agent {

  constructor(dataAgent={}){
    dataAgent.agentType = 'External';
    super(dataAgent);
  }
  // Subclasses should redefine this method
  defaultModel(){
  }
  // Subclasses should redefine this method
  canProvide(producName){
  }
  // Subclasses should redefine this method
  processCallForProposal(message){
  }
  // Subclasses should redefine this method
  processPropose(message){
  }
  // Subclasses should redefine this method
  processAcceptProposal(message){
  }
  // Subclasses should redefine this method
  processRejectProposal(message){
  }
  // Subclasses should redefine this method
  processRefuse(message){
  }
  // Subclasses should redefine this method
  processFailure(message){
  }
  // Subclasses should redefine this method
  processInformDone(message){
  }
  // Subclasses should redefine this method
  processInformResult(message){
  }
}

module.exports = External;