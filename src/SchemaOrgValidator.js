class SchemaOrgValidator {

  isValid(aSchemaObject) {
    if ((aSchemaObject["@context"] != 'http://schema.org') || (aSchemaObject["@type"] == undefined)) {
      return false;
    }
    switch (this.objectToValidate["@type"]) {
        case 'Demand': return this.isValidDemand(aSchemaObject);  
        case 'Offer': return this.isValidOffer(aSchemaObject);
    }
    return false;
  }

  /**
   * aSchemaObject should have this form:
   *  { "@content": "http://schema.org/", 
   *    "@type:": "Demand",
   *    "itemOffered": {
   *        "name": "some string..."
   *     },
   *    "quantity": {
   *        "unitCode": "C62",
   *        "value": X
   *     }
   *  }
   * Where C62 means units according to UN/CEFACT Common Codes for Units of Measurement
   * and X is some number. 
   * @param {*} aSchemaObject 
   */
  isValidDemand(aSchemaObject) {
      let item = aSchemaObject.itemOffered;
      let quantity = aSchemaObject.eligibleQuantity;
      if ((item == undefined) || (quantity == undefined)) {
          return false;
      }
      if (item.name == undefined) {
          return false;
      }
      let unit = quantity.unitCode;
      let value = quantity.value;
      if ((unit != 'C62') || isNaN(value)) {
          return false;
      }
      return true;
  }

  isValidOffer(aSchemaObject) {
      if (! this.isValidDemand(aSchemaObject)) {
          return false;
      };
      if (isNaN(aSchemaObject.price)) {
          return false;
      }
  }
}

module.exports = SchemaOrgValidator;
