"use strict";

class Courier {
  constructor() {
    this.outbox = [];
  }

  /**
   *
   * @param {message} Schedules the message for delivery
   */
  queue(message) {
    this.outbox.push(message);
  }

  /**
   * Deliver the messages
   */
  deliverMessages() {
     // implement it in all subclases.
  }

  cleanQueue(){
    this.outbox = [];
  }
  
}

module.exports = Courier;