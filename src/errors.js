"use strict";

class Errors {
  constructor() {
    this.localErrors = {};
  }

  add(key, message){
    let arrayValue = [message];
    if (this.localErrors.hasOwnProperty(key)){
      arrayValue = this.localErrors[key];
      arrayValue.push(message);
    }
    this.localErrors[key] = arrayValue;
  }

  isEmpty(){
  	for(var key in this.localErrors) {
    	if(this.localErrors.hasOwnProperty(key))
      	return false;
    }
    return true;
  }

  fullMessage(){
  	var res = [];
  	for(var key in this.localErrors) {
  		res.push(this.localErrors[key]);
  	}
  	return res.toString();
  }

  clear(){
    this.localErrors = {};
  }

  messages(){
    let res = {};
    for(var key in this.localErrors) {
      let a = res[key] || [];
      a.push(this.localErrors[key]);
      res[key] = a;
    }
    return res;
  }
}

module.exports = Errors;