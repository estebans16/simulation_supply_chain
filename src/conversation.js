"use strict";
var Validator = require('./validator');
var Message = require('./message');

class Conversation{
  constructor(params={}){
    // formado por la simulacion
    this.id = params.id;
    let some_messages = [];
    if (params.messages != undefined){
      params.messages.forEach(function(message){
        var me = new Message[message.performative](message);;
        some_messages.push(me);
      });
    }
    this.messages = some_messages;
  }

  validator(){
    if (this.avalidator instanceof Validator){
      return this.avalidator;
    }
    this.avalidator = new Validator(this);
    return this.avalidator;
  }

  isValid(){
    this.validator().clearErrors();
    this.validator().present(this.attributesToValidate());
    this.validator().blank(this.attributesToValidate());
    return this.validator().errors.isEmpty();
  }

  errors(){
    return this.validator().errors;    
  }

  attributes(){
    return ["id", "messages"];
  }

  attributesToValidate(){
    return ["id"];
  }

  addMessage(message){
    this.messages.push(message);
  }

  getMessages(){
    return this.messages;
  }

  toJson(){
    let messages = [];
    this.messages.forEach(function(message){
      messages.push(message.toJson());
    });
    let json = {
      "id": this.id,
      "messages": messages
    }
    return json;
  }
}

module.exports = Conversation;