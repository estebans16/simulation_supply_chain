'use strict';
var Persist = require('./persist');
var Agent = require('require-all')(__dirname + '/subclasses_agent');
var Chain = require('./chain');
class PersistDynamoDb extends Persist {
	constructor(){
		super();
		this.aws = require('aws-sdk');
    this.lambda = new this.aws.Lambda({ 
      region: 'us-east-1'
    });
    this.docClient = new this.aws.DynamoDB.DocumentClient({region: 'us-east-1'});
    this.ddb =  new this.aws.DynamoDB({apiVersion: '2012-10-08', region: 'us-east-1'});
	}

	findAgent(id){
		let promise = new Promise((resolve, reject) => {
			this.find(id, 'agents')
			.then(data => {
        resolve(new Agent[data.agentType](data));
			})
			.catch(data => reject(data));
		})
		return promise;
	}

	saveAgent(agent){
		let promise = new Promise((resolve, reject) => {
      if (agent.isPersisted()) {
        this.update(this.updateAgentParams(agent))
        .then(data => resolve(data))
        .catch(data => reject(data));
      }else{
        this.create(this.createAgentParams(agent))
        .then(data => resolve(true))
        .catch(data => {
          agent.errors().add('database', data);
          resolve(data)
        });
      }
    });
    return promise;
	}

	allAgents(){
    let params = {
      TableName: 'agents',
    };
    let promise = new Promise((resolve, reject) => {
      this.docClient.scan(params, function(error, data){
        if (error) {
          reject(error);
        }else {
          let agents = [];
          data.Items.forEach(function(agent){
            agents.push(new Agent[agent.agentType](agent));
          });
          resolve(agents);
        }
      });
    });
    return promise;
	}

	findChain(id){
		let promise = new Promise((resolve, reject) => {
      this.find(id, 'chains')
      .then(chain => {
        this.agentsOfChain(chain.id)
        .then(data => {
          if (data != undefined){
            // let agents = data.map(agent => new Agent(agent));
            let agents = data.map(agent => new Agent[agent.agentType](agent));
             
            chain['agents'] = agents;
          }
          resolve(new Chain(chain));
        })
        .catch(data => reject(data));
      })
      .catch(data => reject(data));
    });
    return promise;
	}

	agentsOfChain(chainId) {
    let params = {
      TableName: 'agents',
      FilterExpression: "chainId = :id",
      ExpressionAttributeValues: {
        ":id": parseInt(chainId)
      }
    };
    let promise = new Promise((resolve, reject) => {
      this.docClient.scan(params, function(error, data){
        if (error) {
          reject(error);
        }else {
          resolve(data.Items.map(item => new Agent[item.agentType](item)));
        }
      });
    });
    return promise;
  }

	saveChain(chain){	
		let promise = new Promise((resolve, reject) => {
      this.saveAgentsOfChain(chain.agents)
      .then(data => {
        if (data){
          this.saveChainData(chain)
          .then(data => resolve(true))
          .catch(error => resolve(false))
        }
      })
      .catch(error => resolve(false))
    });
    return promise;
	}

	allChains(){
    let params = {
      TableName: 'chains'
    };
    var a = this;
    let promise = new Promise((resolve, reject) => {
      this.docClient.scan(params, function(error, data){
        if (error) {
          reject(error);
        }else {
          // if (data.Items.length != 0){
            // a.buildChains(data.Items)
            // .then(data => resolve(data))
            // .catch(data => resolve(data)) 
          // }else{
            // resolve([]);
          // }
          resolve(data.Items)
        }
      });
    });
    return promise;
	}

  createTables(){
    let promise = new Promise((resolve, reject) => {
      let requests = ['agents', 'chains'].map(table => this.createTable(table));
      Promise.all(requests)
      .then(response => {
        resolve(response);
      })
      .catch(error => reject(error))
    });
    return promise
  }

	// private
	find(id, table){
		var id = parseInt(id);
    let params = {
      TableName: table,
      Key: {
        'id': id
      }
    };
    let promise = new Promise((resolve, reject) => {
      this.docClient.get(params, function(error, data){
        if (error) {
          reject(error);
        }else {
          if (data.Item != undefined){
            resolve(data.Item);
          }else{
            reject(data);
          }
        }
      });
    });
    return promise;
	}

	create(params){
    let promise = new Promise((resolve, reject) => {
      this.docClient.put(params, function(error, data){
        if (error) {
          reject(error);
        }else {
          resolve(params.Item);
        }
      });
    });
    return promise;
  }

  update(params){
    let promise = new Promise((resolve, reject) => {
      this.docClient.update(params, function(error, data){
        if (error) {
          reject(error);
        }else {
          resolve(data);
        }
      });
    });
    return promise;
  }

	buildChain(chain){;
	  let promise = new Promise((resolve, reject) => {
	    this.agentsOfChain(chain.id)
	    .then(data => { let res = new Chain(chain);
	                    res.agents = data
	                    resolve(res);
	                  }
	    )
	    .catch(data => reject(data))
	  })
	  return promise;
	}


 	buildChains(chains){
	  let otherPromise =  new Promise((resolve, reject) => {
	    let requests = chains.map(chain => this.buildChain(chain));
	    let otherPromise = Promise.all(requests).then(response => {
	      resolve(response);
	    });

	  })
  	return otherPromise;
	}

	saveAgentsOfChain(agents){
    let promise =  new Promise((resolve, reject) => {
      if (agents.length == 0){
        resolve(true);
      }else{
        let requests = agents.map(agent => {this.saveAgent(agent)});
        Promise.all(requests).then(responses => { 
          resolve(!responses.includes('false'));
        });
      }
    });
    return promise;
  }

  saveChainData(chain){
  	let promise = new Promise((resolve, reject) => {
      if (chain.isPersisted()) {
        this.update(this.updateParamsChain(chain))
        .then(data => resolve(data))
        .catch(error => resolve(error));
      }else{
        this.create(this.createParamsChain(chain))
        .then(data => resolve(true))
        .catch(error => {
          chain.errors().add('database', data);
          resolve(error);
        });
      }
    })
    return promise;
  }

  createParamsChain(chain){
    var params = {
      Item: {
        "id": chain.id,
        "chainName": chain.chainName,
        "description": chain.description,
        "template": chain.template,
        "transactionNumber": 1
      },
      TableName: 'chains'
    };
    return params;
  }

  updateParamsChain(chain){
    var params = {
      TableName: 'chains',
        Key:{
            "id": chain.id
        },
        UpdateExpression: "set template=:p, description=:a, chainName=:c, transactionNumber=:MAX  ",
        ConditionExpression: 'transactionNumber < :MAX',
        ExpressionAttributeValues:{
            ":p": chain.template,
            ":a": chain.description,
            ":c": chain.chainName,
            ':MAX': chain.transactionNumber + 1
        },
        ReturnValues:"UPDATED_NEW",
    };
    return params;
  }

  createAgentParams(agent){
    let conversations = this.paramsConversations(agent.conversations);
    var params = {
      Item: {
        "id": agent.id,
        "agentName": agent.agentName,
        "conversations": conversations,
        "active": agent.active,
        "transactionNumber": 1,
        "chainId": agent.chainId,
        "uri": agent.uri,
        "is_external": agent.is_external,
        "agentType": agent.agentType,
        "userStore": agent.userStore,
        "suppliers": agent.suppliers
      },
      TableName: 'agents'
    };
    return params;
  }

  updateAgentParams(agent){
    let conversations = this.paramsConversations(agent.conversations);
    let updateExpression = "set conversations=:a, active=:b, transactionNumber=:MAX, " + 
                           "uri=:u, is_external=:e, agentName= :c, agentType= :d, userStore= :f, suppliers= :h";
    var params = {
      TableName: 'agents',
        Key:{
            "id": agent.id
        },
        UpdateExpression: updateExpression ,
        ConditionExpression: 'transactionNumber < :MAX',
        ExpressionAttributeValues:{
            ":a": conversations,
            ":b": agent.active,
            ':MAX': agent.transactionNumber + 1,
            ':u': agent.uri,
            ':e': agent.is_external,
            ':c': agent.agentName,
            ':d': agent.agentType,
            ':f': agent.userStore,
            ':h': agent.suppliers
        },
        ReturnValues:"UPDATED_NEW",
    };
    return params
  }

  paramsConversations(conversations){
    const prop = 'avalidator'
    let params = conversations.map(conversation => {
      let newConversation = this.deleteProperty(conversation, prop);
      let messages = conversation.messages.map(message => {
        return this.deleteProperty(message, prop);
      })
      newConversation.messages = messages;
      return newConversation
    })
    return params;
  }

  deleteProperty(originalObject, property){
    let newObject =  Object.keys(originalObject).reduce((object, key) => {
      if (key !== property) {
        object[key] = originalObject[key]
      }
        return object
    }, {})
    return newObject;
  }

  createTable(tableName){
     var params = {
      AttributeDefinitions: [
        {
          AttributeName: 'id',
          AttributeType: 'N'
        }
      ],
      KeySchema: [
        {
          AttributeName: 'id',
          KeyType: 'HASH'
        }
      ],
      ProvisionedThroughput: {
        ReadCapacityUnits: 1,
        WriteCapacityUnits: 1
      },
      TableName: tableName,
      StreamSpecification: {
        StreamEnabled: false
      }
    };
    // Call DynamoDB to create the table
    let promise = new Promise((resolve, reject) => {
      this.ddb.createTable(params, function(err, data) {
        if (err) {
          reject(err);
        } else {
          resolve(data);
        }
      });
    });
    return promise;
  }

  tableExist(tableName){
    let promise = new Promise((resolve, reject) => {
      this.ddb.listTables(null, function(err, data) {
        if (err) {
          reject(err);
        }else{ 
          if (!data['TableNames'].includes(tableName)){
            reject(false); //No existe la tabla
          }else{
            resolve(true); //existe la tabla
          }
        }
      });
    });
    return promise
  }

  conversationsOfChain(chainId){
    let promise = new Promise((resolve, reject) => {
      this.agentsOfChain(chainId)
      .then(agents => {
        //Obtengo las conversaciones y las uno en un unico array
        let conversations = agents.map(agent => agent.conversations).reduce((acc, val) => acc.concat(val));
        resolve(conversations);
      })
      .catch(error =>reject(error))
    });
    return promise
  }

  destroy(elementId, table){
    let promise = new Promise((resolve, reject) => {
      var params = {
        TableName: table,
        Key: { id: parseInt(elementId) }
      };
      this.docClient.delete(params, function(err, data) {
        if (err) { 
          reject(err); // an error occurred
        }else {
          resolve(data); // successful response
        }
      });
    });
    return promise;
  }
}


module.exports = PersistDynamoDb;