'use strict';
let Message = require('./message');
let Conversation = require('./conversation');
let Validator = require('./validator');
var CourierLambda = require('./courierLambda');

class Agent{

  constructor(dataAgent={}){
    this.agentName = dataAgent.agentName;
    let result = [];
    if (dataAgent.conversations != undefined) {
      dataAgent.conversations.forEach(function(conversation){ result.push(new Conversation(conversation)) })
    }
    this.conversations = result;
    this.active = dataAgent.active;
    this.transactionNumber = dataAgent.transactionNumber || 0; //Si es 0 no se encuentra en la BD
    this.id = dataAgent.id || this.generateRandomId();
    this.chainId = dataAgent.chainId;
    this.uri = dataAgent.uri || this.defaultUri() + '/agents/' + this.id;
    this.is_external = dataAgent.is_external;
    this.agentType = dataAgent.agentType;
    this.userStore = dataAgent.userStore || {};
    this.suppliers = dataAgent.suppliers || {};
    this.courier = new CourierLambda(); 
  }

  validator(){
    if (this.avalidator instanceof Validator){
      return this.avalidator;
    }
    this.avalidator = new Validator(this);
    return this.avalidator;
  }

  isValid(){
    this.validator().clearErrors();
    this.validator().present(this.attributesToValidate());
    this.validator().blank(this.attributesToValidate().filter(atr => atr !== 'is_external'));
    return this.validator().errors.isEmpty();
  }

  errors(){
    return this.validator().errors;    
  }

  attributes(){
    return ["id", "agentName", "stock", "providers", "conversations", "active", "chainId", "uri",
            "is_external", "transactionNumber", 'dataAgent', 'userStore', 'suppliers'];
  }

  attributesToValidate(){
    return ["id", "agentName", "chainId", "uri", "is_external"];
  }

  toJson(){
    let conversations = []
    this.conversations.forEach(function(conversation){
      conversations.push(conversation.toJson());
    })
    let a = {
              "id": this.id,
              "agentName": this.agentName,
              "active": this.active,
              "stock": this.stock,
              "conversations": conversations,
              "chainId": this.chainId,
              "transactionNumber": this.transactionNumber,
              "is_external": this.is_external,
              "uri": this.uri,
              "agentType": this.agentType,
              "userStore": this.userStore,
              "suppliers": this.suppliers
            };
    return a;
  }

  addConversation(conversation){
    this.conversations.push(conversation);
  }

  updateAttributes(params={}){
    for (let key in params){
      if (params[key] != undefined){
        this[key] = params[key];
      };
    }
  }

  addMessageToConversation(message){
    let conversation = this.getConversation(message.conversationId);
    //No existe conversacion
    if (conversation == undefined && message.performative != 'CallForProposal'){
      return false;
    }else{
      if (conversation != undefined){
        // conversacion existen
        // this.conversations[resIndex].addMessage(message);  
        conversation.addMessage(message);
      }else {
        // nueva conversacion
        if (message.conversationId == undefined) {
          message.conversationId = this.generateRandomId();
        }
        let conversation = new Conversation({id: message.conversationId});
        conversation.addMessage(message);
        this.addConversation(conversation);
      }
      return true;
    }
  }

  getConversation(conversationId){
    return this.conversations.find(c => c.id == conversationId);
  }

  processMessage(message){
    this['process' + message.performative](message);
  }

  generateRandomId(){
    let EPOCH = 1300000000000;
    let ts = new Date().getTime() - EPOCH; // limit to recent
    let randid = Math.floor(Math.random() * 512);
    ts = (ts * 64);   // bit-shift << 6
    return (ts * 512) + (randid % 512);
  }

  isPersisted(){
    return !(this.transactionNumber == 0);
  }

  clone(){
    let params = {agentName: this.agentName,
                  stock: this.stock,
                  conversations: this.conversations,
                  active: this.active,
                  is_external: this.is_external,
                  agentType: this.agentType,
                  userStore: this.userStore,
                  suppliers: this.suppliers
                  };
    if (this.is_external) {
      params.uri = this.uri;
    }
    let clone = new Agent(params);

    return clone;
  }

  getStore(){
    return this.userStore;
  }

  setStore(store){
    this.userStore = store;
  }

  getSuppliersUri(productName){
    return this.suppliers[productName];
  }

  haveSuppliers(){
    return this.suppliers.length != 0
  }

  getUri(){
    return this.uri;
  }

  getProducts(){
    return Object.keys(this.getStore()['products']);
    // this.products;
  }

  newCallForProposal(receiver, content){
    let cfp = new Message({sender: this.getUri(), receiver: receiver, content: content, 
                           performative: 'CallForProposal', conversationId: this.generateRandomId()});
    return cfp;
  }
  
  canProvide(producName){
    return this.getProducts().includes(producName);
  }

  defaultUri(){
    let Configuration = require('./config/configuration');
    let config  = new Configuration();
    return config.defaultUri;
  }

  queue(message){
    this.addMessageToConversation(message);
    this.courier.queue(message);
  }

  comment(){
   return 'this class has no commend - define the comment() method to return a comment';
  }

  // Subclasses should redefine this method
  defaultStore(){
    return {};
  }
  // Subclasses should redefine this method
  processCallForProposal(message){
  }
  // Subclasses should redefine this method
  processPropose(message){
  }
  // Subclasses should redefine this method
  processAcceptProposal(message){
  }
  // Subclasses should redefine this method
  processRejectProposal(message){
  }
  // Subclasses should redefine this method
  processRefuse(message){
  }
  // Subclasses should redefine this method
  processFailure(message){
  }
  // Subclasses should redefine this method
  processInformDone(message){
  }
  // Subclasses should redefine this method
  processInformResult(message){
  }

}

module.exports = Agent;