// Variable de entorno, indica si el frontend utiliza el ambiente AWS o el ambiente LOCAL
let ENV = 'AWS';

//URL para acceder a los agentes y cadenas en AWS
let API_URL_AWS = 'https://wcch7nt8dg.execute-api.us-east-1.amazonaws.com/dev/';
let API_AGENTS_URL_AWS = API_URL_AWS+'agents';
let API_CHAINS_URL_AWS = API_URL_AWS+'chains';

//URL para acceder a los agentes y cadenas en LOCAL
let API_URL_LOCAL = 'http://localhost:3000/';
let API_AGENTS_URL_LOCAL = API_URL_LOCAL+'agents';
let API_CHAINS_URL_LOCAL = API_URL_LOCAL+'chains';

//Por defecto el frontend se inicia utilizando los servicios de AWS
let API_URL = API_URL_AWS;
let API_AGENTS_URL = API_AGENTS_URL_AWS;
let API_CHAINS_URL = API_CHAINS_URL_AWS;
//let API_AGENTS_URL = API_AGENTS_URL_LOCAL;
//let API_CHAINS_URL = API_CHAINS_URL_LOCAL;

window.subclasses = [];

$(document).ready(function () {
    tableChainInitialize();
    getSubclassesOfAgents();

    $('#new_chain_button').unbind("click").click(function(){
        newChainEvent();
    });

    $('#new_agent_button').unbind("click").click(function(){
        newAgentEvent();
    });

    $('#report_button').unbind("click").click(function(){
        reportEvent();
    });

    $('#nav').html('<li class="breadcrumb-item active" aria-current="page">Home <i class="fa fa-home"></i></li>');

});

function toggleEnv() {
    if (ENV == 'AWS') {
        API_URL = API_URL_LOCAL;
        API_AGENTS_URL = API_AGENTS_URL_LOCAL;
        API_CHAINS_URL = API_CHAINS_URL_LOCAL;
        ENV = 'LOCAL';
        $('#load_data').show();
    } else {
        API_URL = API_URL_AWS;
        API_AGENTS_URL = API_AGENTS_URL_AWS;
        API_CHAINS_URL = API_CHAINS_URL_AWS;
        ENV = 'AWS';
        $('#load_data').hide();
    }

    goHome();
}

function goHome() {
    $('#nav').html('<li class="breadcrumb-item active" aria-current="page">Home <i class="fa fa-home"></i></li>');
    $('#chainsTableBody').empty();
    $('#chainSection').hide();
    $('#agentSection').hide();
    $('section').hide();
    $('#chainModal').hide();
    $('#agentModal').hide();
    $('#messageModal').hide();
    $('#chainsSection').show();
    tableChains();
    getSubclassesOfAgents();
}

function showErrors(errors) {
    let msg = '';
    $.each(errors, function(i,error){
        msg += error + '\n';
    });
    alert(msg);
}

function IsJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

function loadData() {
    if (confirm('Are you sure load data from seed file?')) {
        $.ajax({
            type: 'POST',
            url: API_URL+'loadData',
            dataType: 'html',
            success: function(data){
                console.log(data);
                goHome();
            },
            error: function( jqXHR, textStatus, errorThrown){
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
                showErrors(jqXHR.responseJSON);
            }
        });
    }
}

//Deshabilitar elemento
function disabled(element) {
    element.attr('disabled','disabled');
}

//Habilitar elemento
function enabled(element) {
    element.removeAttr('disabled');
}

//Inicializa la tabla de cadenas
function tableChainInitialize() {
    $('<div class="loading text-center"><h5><i class="fa fa-cog fa-spin fa-3x fa-fw"></i><div class="">Loading...</div></h5></div>').appendTo('#collapseList');
    $.ajax({
        type: 'GET',
        url: API_CHAINS_URL,
        success: function(data){
            console.log(data);
            window.chains = [];
            var table = $('#chainsTableBody').empty();
            content = '';
            $.each(data, function(i,item) {
                window.chains.push(item);
                content += '<tr>';
                content += '<td>' + item.chainName + '</td>';
                content += '<td>' + item.description + '</td>';
                if (item.template) {state = 'Yes'} else {state = 'No'}
                content += '<td>' + state + '</td>';
                content += '<td>' + goChainButton(item) + editChainButton(item) + cloneChainButton(item) + cleanChainButton(item) + ' </td>';
                content += '</tr>';
            });

            $('#chainsTableBody').append(content);
            window.chainsTable = $('#chainsTable').DataTable({
                "order": [[ 0, "asc" ]],
                "initComplete": function( settings, json ) {
                    $('div.loading').remove();
                }
            });
        
            $('#chainsTable tbody').unbind('dblclick').on('dblclick', 'tr', function () {
                var data = table.row( this ).data();

                $('#btn_go_'+data[0]).trigger('click');
            } );

        },
        error: function( jqXHR, textStatus, errorThrown){
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
            $('div.loading').remove();
            showErrors(jqXHR.responseJSON);
        }
    });
}

//devuelve el html del boton de edicion
function goChainButton(chain){
    return '<a id="btn_go_' + chain.id +  '" data-chain="' + chain.id +  '" class="btn btn-success btn-sm gochain" title="Go chain" onClick="goChainEvent('+chain.id+')"><i class="fa fa-link"></i></a>';
}

//devuelve el html del boton de edicion
function editChainButton(chain){
    return '<a id="btn_edit_' + chain.id +  '" data-chain="' + chain.id +  '" class="btn btn-warning btn-sm editchain" title="Edit chain" onClick="editChainEvent('+chain.id+')"><i class="fa fa-edit"></i></a>';
}

//devuelve el html del boton de clonacion
function cloneChainButton(chain){
    return '<a id="btn_clone_' + chain.id +  '" data-chain="' + chain.id +  '" class="btn btn-primary btn-sm clonechain" title="Clone chain" onClick="cloneChainEvent('+chain.id+')"><i class="far fa-clone"></i></a>';
}

//devuelve el html del boton de limpiar conversaciones
function cleanChainButton(chain){
    return '<a id="btn_clean_' + chain.id +  '" data-chain="' + chain.id +  '" class="btn btn-secondary btn-sm clonechain" title="Clean chain" onClick="cleanChainEvent('+chain.id+')"><i class="fas fa-broom"></i></a>';
}

//vacio el formulario
function clearChainForm(form){
    form.trigger("reset");
    form.find('#toggle_template').removeClass('active');
    form.find('#toggle_template').attr('aria-pressed',false);
}

//vacio el formulario
function clearAgentForm(form){
    form.trigger("reset");
    form.find('#toggle_external').removeClass('active');
    form.find('#toggle_external').attr('aria-pressed',false);
}

//transformo el formulario a objeto
function formChainToHash(form){
    template = form.find("#toggle_template").hasClass('active') ;
    return { "chainName": form.find("#chainName").val(),  
            "template": template,
            "description": form.find("#description").val()
    };
}

//refresco tabla de cadenas
function tableChains() {
    $('<div class="loading text-center"><h5><i class="fa fa-cog fa-spin fa-3x fa-fw"></i><div class="">Loading...</div></h5></div>').appendTo('#collapseList');
    $.ajax({
        type: 'GET',
        url: API_CHAINS_URL,
        success: function(data){
            window.chains = [];
            var table = $('#chainsTableBody').empty();
            content = '';
            $.each(data, function(i,item) {
                window.chains.push(item);
                content += '<tr>';
                content += '<td>' + item.chainName + '</td>';
                content += '<td>' + item.description + '</td>';
                if (item.template) {state = 'Yes'} else {state = 'No'}
                content += '<td>' + state + '</td>';
                content += '<td>' + goChainButton(item) + editChainButton(item) + cloneChainButton(item) + cleanChainButton(item) + ' </td>';
                content += '</tr>';
            });

            window.chainsTable.destroy();
            $('#chainsTableBody').html('');
            $('#chainsTableBody').append(content);
            window.chainsTable = $('#chainsTable').DataTable({
                "order": [[ 0, "asc" ]],
                "initComplete": function( settings, json ) {
                    $('div.loading').remove();
                }
            });
            
            $('div.loading').remove();

        },error: function( jqXHR, textStatus, errorThrown){
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
            $('div.loading').remove();
        } 
    });
}

//evento para editar una cadena
function editChainEvent(id_chain){

    let modal = $('#chainModal');
    let form = $("#chainForm");
    populateChainForm(form, id_chain);

    $('#modalChainTitle').html('Edit Supply Chain');
    form.unbind("submit").submit(function(e){
        e.preventDefault();
        let form = $(this);
        let adata = JSON.stringify(formChainToHash(form));
        console.log(adata);
        $.ajax({
            type: 'PATCH',
            url: API_CHAINS_URL,
            data: adata,
            contenType: "application/json",
            success: function(data){
                console.log(data);
                tableChains();
                clearForm(form);
                let modal = $('#chainModal');
                modal.modal('hide');
            },error: function( jqXHR, textStatus, errorThrown){
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
                showErrors(jqXHR.responseJSON);
            } 
        });
    });
    $("#formChainButton").unbind("click").click(function(){
        disabled($('#chainModal .btn'));
        sendEditChainForm(id_chain);
    });
    modal.modal('show');
}

function goChainEvent(id_chain) {

        let data = window.chains.find(x => x.id === id_chain);
        if (data !== undefined) {
            $('section').hide();
            window.chain = data;
            $('.chainNameTitle').html(data.chainName);
            if (data.template === true) {
                $('#templateChain').html('<h6><div class="badge badge-pill badge-success"><i class="fa fa-check"></i> Template</div></h6>');
                disabled($('#simulate_chain_button'));
            } else {
                $('#templateChain').html('');
                enabled($('#simulate_chain_button'));
            }
            $('#descriptionChain').html(data.description);

            $('#nav').html('<li class="breadcrumb-item"><a href="#" onClick="goHome();">Home <i class="fa fa-home"></i></a></li><li class="breadcrumb-item active" aria-current="page">'+data.chainName+' <i class="fa fa-link"></i></li>');

            $('#chainSection').fadeIn();

            $.ajax({
                type: 'GET',
                url: API_CHAINS_URL + '/' + window.chain.id + '/agents',
                success: function(data){
                    window.chain.agents = data;
                    tableAgentsInitialize(data);        
                },error: function( jqXHR, textStatus, errorThrown){
                    console.log(jqXHR);
                    console.log(textStatus);
                    console.log(errorThrown);
                    showErrors(jqXHR.responseJSON);
                }
            });

        } else {
            alert('Chain not found.');
        }
}

//evento para crear una nueva cadena
function newChainEvent(){
    let modal = $('#chainModal');
    let form = $("#chainForm");
    $('#modalChainTitle').html('New Supply Chain');
    clearChainForm(form);
    form.unbind("submit").submit(function(e){
        e.preventDefault();
        let form = $(this);
        let adata = JSON.stringify(formChainToHash(form));
        console.log(adata);
        $.ajax({
            type: 'POST',
            url: API_CHAINS_URL,
            data: adata,
            contenType: "application/json",
            success: function(data){
                console.log(data);
                tableChains();
                clearForm(form);
                let modal = $('#chainModal');
                modal.modal('hide');
            },error: function( jqXHR, textStatus, errorThrown){
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
                showErrors(jqXHR.responseJSON);
            } 
        });
    });
    $("#formChainButton").unbind("click").click(function(){
        disabled($('#chainModal .btn'));
        sendNewChainForm();
    });
    modal.modal('show');
}

function sendNewChainForm() {
    let form = $("#chainForm");
    console.log(formChainToHash(form));
    let adata = JSON.stringify(formChainToHash(form));
    console.log(adata);
    $.ajax({
        type: 'POST',
        url: API_CHAINS_URL,
        data: adata,
        contenType: "application/json",
        success: function(data, status, jqXHR){
            console.log(data);
            console.log(status);
            console.log(jqXHR);
            tableChains();
            if (status == 'success') {
                alert(data);
            } else {
                showErrors(data);
            }
            clearChainForm(form);
            let modal = $('#chainModal');
            modal.modal('hide');
            enabled($('#chainModal .btn'));
        },
        error: function( jqXHR, textStatus, errorThrown){
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
            showErrors(jqXHR.responseJSON);
            enabled($('#chainModal .btn'));
        }
    });
}

function sendEditChainForm(id_chain) {
    let form = $("#chainForm");
    console.log(formChainToHash(form));
    let adata = JSON.stringify(formChainToHash(form));
    console.log(adata);
    $.ajax({
        type: 'PATCH',
        url: API_CHAINS_URL+ '/' +id_chain,
        data: adata,
        contenType: "application/json",
        success: function(data, status, jqXHR){
            console.log(data);
            console.log(status);
            console.log(jqXHR);
            tableChains();
            if (status == 'success') {
                alert(data);
            } else {
                showErrors(data);
            }
            clearChainForm(form);
            let modal = $('#chainModal');
            modal.modal('hide');
            enabled($('#chainModal .btn'));
        },
        error: function( jqXHR, textStatus, errorThrown){
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
            showErrors(jqXHR.responseJSON);
            enabled($('#chainModal .btn'));
        }
    });
}

//seteo el formulario
function populateChainForm(form, id_chain){
    let data = window.chains.find(x => x.id === id_chain);
    console.log(data);
    form.find("#chainName").val(data.chainName);

    if (data.template === true) {
        form.find("#toggle_template").addClass('active');
    } else {
        form.find("#toggle_template").removeClass('active');
    }

    form.find("#description").val(data.description);
}

//evento para clonar una cadena
function cloneChainEvent(id_chain){

    let data = window.chains.find(x => x.id === id_chain);
    console.log(data);

    if (confirm('Are you sure to clone the chain ' + data.chainName + '?')) {
        $.ajax({
            type: 'POST',
            url: API_CHAINS_URL + '/' + id_chain + '/clone',
            contenType: "application/json",
            success: function(data, status, jqXHR){
                console.log(data);
                console.log(status);
                console.log(jqXHR);
                tableChains();
                if (status == 'success') {
                    alert(data);
                } else {
                    showErrors(data);
                }
            },
            error: function( jqXHR, textStatus, errorThrown){
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
                showErrors(jqXHR.responseJSON);
            }
        });
    }
}

//evento para limpiar una cadena
function cleanChainEvent(id_chain){

    let data = window.chains.find(x => x.id === id_chain);
    console.log(data);

    if (confirm('Are you sure to clean the chain ' + data.chainName + '?')) {
        $.ajax({
            type: 'POST',
            url: API_CHAINS_URL + '/' + id_chain + '/clear',
            contenType: "application/json",
            success: function(data, status, jqXHR){
                console.log(data);
                console.log(status);
                console.log(jqXHR);
                tableChains();
                if (status == 'success') {
                    alert(data);
                } else {
                    showErrors(data);
                }
            },
            error: function( jqXHR, textStatus, errorThrown){
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
                showErrors(jqXHR.responseJSON);
            }
        });
    }
}

/******************************************************************************************************************************
 * FIN CADENA 
 *****************************************************************************************************************************/

 /*****************************************************************************************************************************
  * INICIO AGENTE
  ****************************************************************************************************************************/

//refresco tabla de agentes
function tableAgents(data) {
    window.agents = [];
    var table = $('#agentsTableBody').empty();
    content = '';
    $.each(data, function(i,item) {
        window.agents.push(item);
    });
    $.each(window.chains, function(i,chain) {
        //Busco la cadena actual y le seteo los nuevos agentes
        if (chain.id == window.chain.id) {
            chain.agents = window.agents;
            return false;
        }
    });
    $.each(window.agents, function(i,item) {
        content += '<tr>';
        content += '<td>' + item.agentName + '</td>';
        content += '<td>' + item.agentType + '</td>';

        let supplier = {};
        let suppliersName = [];
        $.each(item.suppliers, function(i,uri) {
            supplier = window.agents.find(x => x.uri == uri);
            if (supplier != undefined && supplier != 'undefined') {
                suppliersName.push(supplier.agentName);
            }
        });

        content += '<td>' + suppliersName + '</td>';
        if (item.active) {state = 'Active'} else {state = 'Inactive'}
        content += '<td>' + state + '</td>';
        content += '<td>' + goAgentButton(item) + sendMessageButton(item) + editAgentButton(item) + ' </td>';
        // + toggleAgentButton(item) 
        content += '</tr>';
    });

    $('#agentsTableBody').append(content);

}

//refresco tabla de agentes
function tableAgentsInitialize(data) {
    window.agents = [];
    var table = $('#agentsTableBody').empty();
    content = '';
    $.each(data, function(i,item) {
        window.agents.push(item);
    });
    $.each(window.agents, function(i,item) {
        content += '<tr>';
        content += '<td>' + item.agentName + '</td>';
        content += '<td>' + item.agentType + '</td>';

        let supplier = {};
        let suppliersName = [];
        $.each(item.suppliers, function(i,uri) {
            supplier = window.agents.find(x => x.uri == uri);
            if (supplier != undefined && supplier != 'undefined') {
                suppliersName.push(supplier.agentName);
            }
        });

        content += '<td>' + suppliersName + '</td>';
        if (item.active) {state = 'Active'} else {state = 'Inactive'}
        content += '<td>' + state + '</td>';
        content += '<td>' + goAgentButton(item) + sendMessageButton(item) + editAgentButton(item) + ' </td>';
        // + toggleAgentButton(item) + ' </td>';
        content += '</tr>';
    });

    if ($.fn.DataTable.isDataTable( '#agentsTable' )) {
        window.agentsTable.destroy();
        $('#agentsTableBody').html('');
    }
    $('#agentsTableBody').append(content);
    window.agentsTable = $('#agentsTable').DataTable({
        "order": [[ 0, "asc" ]]
    });

}

//devuelve el html del boton de ir a un agente
function goAgentButton(agent){
    return '<a id="btn_go_agent_' + agent.id +  '" data-agent="' + agent.id +  '" class="btn btn-success btn-sm goAgent" title="Go agent" onClick="goAgentEvent('+ agent.id +')"><i class="fa fa-user"></i></a>';
}

//devuelve el html del boton de edicion
function editAgentButton(agent){
    return '<a id="btn_edit_' + agent.id +  '" data-agent="' + agent.id +  '" class="btn btn-warning btn-sm editAgent" title="Edit agent" onClick="editAgentEvent('+ agent.id +')"><i class="fa fa-edit"></i></a>';
}

// Si esta inactivo, boton para activar, caso contrario boton para inhabilitar
function toggleAgentButton(agent) {
    if (agent.active){
        return '<a data-agent="' + agent.id +  '" class="btn btn-danger btn-sm removeAgent" title="Disable agent" onClick="removeAgentEvent('+ agent.id +')"><i class="fa fa-trash-alt"></i></a>'; 
    }
    else{
        return '<a data-agent="' + agent.id +  '" class="btn btn-success btn-sm enableAgent" title="Enable agent" onClick="enableAgentEvent('+ agent.id +')"><i class="fa fa-recycle"></i></a>';
    }
}

//devuelve el html del boton de envio de mensajes
function sendMessageButton(agent){
    if (window.chain.template) {
        return '';
    } else {
        return '<a id="btn_send_' + agent.id +  '" data-agent="' + agent.id +  '" class="btn btn-primary btn-sm sendMessage" title="Send message" onClick="sendMessageFormEvent('+ agent.id +')"><i class="fas fa-paper-plane"></i></a>';
    }
}

//transformo el formulario a objeto
function formAgentToHash(form){
    let active = false;
    if (form.find("#active").prop('checked')) {
        active = true ;  
    }
    let external = form.find("#toggle_external").hasClass('active') ;
    let uri = '';
    if (external) {
        uri = form.find("#urlAgent").val();
    }

    let supp = form.find('#suppliers');
    let count = supp.children().length;
    let suppliers = {};
    for (let index = 0; index < count; index++) {
        if (supp.find('#supply_'+index+'_name').val().trim() != '' && supp.find('#supply_'+index+'_providers').val().length > 0) {
            suppliers[supp.find('#supply_'+index+'_name').val()] = supp.find('#supply_'+index+'_providers').val();
        }
    }
    let content = JSON.parse(form.find("#content").val());
    let hash = {"agentName": form.find("#agentName").val(),  
                "agentType": form.find("#agentType").val(),
                "active": active,
                "chainId": window.chain.id,
                "is_external": external,
                "suppliers": suppliers,
                "userStore": content
            };
    if (uri != '') {
        hash['uri'] = uri;
    }

    let subclase = window.subclasses.find(hash => hash['class'] == form.find("#agentType").val());

    $.each( subclase['modelAttribute'], function( key, value ) {
        hash['model'][key] = form.find("#"+key).val();
    });

    return hash;
}

//seteo el select de proveedores. optionsSelected es opcional, solo si tiene proveedores seteados
function selectAgents(form, optionsSelected=[]){
    //vacio el select
    var select = form.find('#providers').empty();
    //completo los proveedoresº
    $.each(window.agents, function(i,item) {
        select.append( '<option value="'+ item.id +'">'
                            + item.agentName
                        + '</option>' ); 
    });
    //si tiene proveedores los pongo como selected
    $.each(optionsSelected, function(i,e){
        form.find("#providers option[value='" + e + "']").prop("selected", true);
    })
    //tranformo a chosen y actualizo
    setTimeout(function(){
        select.chosen();
    },500);
    select.trigger('chosen:updated');
}

//seteo el select de proveedores. optionsSelected es opcional, solo si tiene proveedores seteados
function selectProviders(select, optionsSelected=[]){
    //vacio el select
    var select = select.empty();
    //completo los proveedoresº
    $.each(window.agents, function(i,item) {
        select.append( '<option value="'+ item.uri +'">'
                            + item.agentName
                        + '</option>' ); 
    });
    //si tiene proveedores los pongo como selected
    $.each(optionsSelected, function(i,e){
        select.find("option[value='" + e + "']").prop("selected", true);
    })
    //tranformo a chosen y actualizo
    setTimeout(function(){
        select.chosen();
    },500);
    select.trigger('chosen:updated');
}

function sendNewAgentForm() {
    let form = $("#agentForm");
    let adata = JSON.stringify(formAgentToHash(form));
    console.log(adata);
    $.ajax({
        type: 'POST',
        url: API_AGENTS_URL,
        data: adata,
        contenType: "application/json",
        success: function(data,status){
            console.log(data);
            let msg = data;
            if (status == 'success') {
                $.ajax({
                    type: 'GET',
                    url: API_CHAINS_URL + '/' + window.chain.id + '/agents',
                    success: function(data){
                        alert(msg);
                        console.log(data);
    
                        tableAgents(data);
                        clearAgentForm(form);
                        let modal = $('#agentModal');
                        modal.modal('hide');
                        enabled($('#agentModal .btn'));
                    },error: function( jqXHR, textStatus, errorThrown){
                        console.log(jqXHR);
                        console.log(textStatus);
                        console.log(errorThrown);
                        showErrors(jqXHR.responseJSON);
                        enabled($('#agentModal .btn'));
                    }
                });
            } else {
                showErrors(data);
                enabled($('#agentModal .btn'));
            }
        },error: function( jqXHR, textStatus, errorThrown){
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
            showErrors(jqXHR.responseJSON);
            enabled($('#agentModal .btn'));
        }
    });
}

//evento para crear un nuevo agente
function newAgentEvent(){
    let modal = $('#agentModal');
    let form = $("#agentForm");
    $('#modalAgentTitle').html('New Agent');
    clearChainForm(form);
    selectAgents(form);
    //limpio los suppliers para luego generar los que corresponda
    form.find('#suppliers').html('');
    newSupplyEvent('agentForm');
    selectAgentTypes(form);
    form.find("#availableAtDate").val(new Date().toISOString().slice(0,10));
    form.find("#active").prop('checked', true);
    form.unbind("submit").submit(function(e){
        e.preventDefault();
        let form = $(this);
        let adata = JSON.stringify(formAgentToHash(form));
        console.log(adata);
        $.ajax({
            type: 'POST',
            url: API_AGENTS_URL,
            data: adata,
            contenType: "application/json",
            success: function(data){
                console.log(data);
                tableAgents();
                clearChainForm(form);
                let modal = $('#agentModal');
                modal.modal('hide');
            },error: function( jqXHR, textStatus, errorThrown){
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
                showErrors(jqXHR.responseJSON);
            }
        });
    });
    enabled(form.find('#agentName'));
    enabled(form.find('#agentType'));
    enabled(form.find('#providers'));
    enabled(form.find("#toggle_external"));
    enabled(form.find("#urlAgent"));
    form.find("#toggle_external").removeClass('active');
    form.find("#divURL").hide();

    let subclase = window.subclasses.find(hash => hash['class'] == form.find("#agentType").val());

    $.each( subclase['modelAttribute'], function( key, value ) {
        enabled(form.find("#"+key));
    });

    $("#formAgentButton").unbind("click").click(function(){
        disabled($('#agentModal .btn'));
        sendNewAgentForm();
    });
    modal.modal('show');   
}

function newSupplyEvent(form, supply = '', optionsSelected=[]) {
    let suppliers = $('#'+form).find('#suppliers');
    let readonly = '';
    let disabled = '';
    let display = '';
    if (suppliers.attr('readonly')) {
        readonly = 'readonly';
        disabled = 'disabled';
        display = 'display: none;';
    }
    let count = suppliers.children().length;
    let labelSupply = '';
    let labelSupplier = '';
    let labelBtn = '';
    let margin = '-2';
    if (count == 0) {
        labelSupply = "<label>Supplies</label>";
        labelSupplier = "<label>Suppliers</label>";
        labelBtn = "<label>&emsp;</label>";
        margin = '0';
    }
    let html = "<div id=\"supply_"+count+"\" name=\"supply_"+count+"\" class=\"row divURL\" style=\"margin-top: "+margin+"%;\">"
    + "<div class=\"col-md-5\">"
    + "<div class=\"form-group\">"
    + labelSupply
    + "<input type=\"text\" class=\"form-control\" id=\"supply_"+count+"_name\" name=\"supply_"+count+"_name\" placeholder=\"Supply\" "+readonly+">"
    + "</div>"
    + "</div>"
    + "<div class=\"col-md-6\">"
    + "<div class=\"form-group\">"
    + labelSupplier
    + "<select multiple class=\"form-control providers\" id=\"supply_"+count+"_providers\" name=\"supply_"+count+"_providers[]\" "+disabled+">"
    + "</select>"
    + "</div>"
    + "</div>"
    + "<div class=\"col-md-1\" style=\"padding-left: 0%; "+display+"\">"
    + "<div class=\"form-group\">"
    + labelBtn
    + "<div class=\"btn btn-sm btn-primary\" onClick=\"newSupplyEvent('"+form+"');\"><i class=\"fa fa-plus\"></i></div>"
    + "</div>"
    + "</div>"
    + "</div>";
    suppliers.append(html);
    $('#supply_'+count+'_name').val(supply);
    selectProviders($('#'+form).find('#supply_'+count+'_providers'), optionsSelected);
}

//seteo el formulario
function populateAgentForm(form, id_agent, formId = 'agentForm'){
    let data = window.agents.find(x => x.id === id_agent);
    console.log(data);
    form.find("#id").val(data.id);
    form.find("#agentName").val(data.agentName);

    setTimeout(function(){
        form.find("#active").prop('checked', data.active);
    },500);
    if (data.is_external === true) {
        form.find("#toggle_external").addClass('active');
        form.find("#urlAgent").val(data.uri);
        form.find('.divURL').hide();
        form.find("#divURL").show();
    } else {
        form.find("#toggle_external").removeClass('active');
        form.find("#urlAgent").val(data.uri);
        form.find('.divURL').show();
        form.find("#divURL").hide();
    }
    selectAgents(form, data.providers);

    //limpio los suppliers para luego generar los que corresponda
    form.find('#suppliers').html('');
    // Si no tengo suppliers genero los inputs vacios
    if (Object.keys(data.suppliers).length <= 0) {
        newSupplyEvent(formId);
    } else {
        Object.keys(data.suppliers).forEach(index => {
            newSupplyEvent(formId, index, data.suppliers[index]);
        })
    }
    selectAgentTypes(form, data.agentType);
    let subclase = window.subclasses.find(hash => hash['class'] == data.agentType);

    $.each( subclase['modelAttribute'], function( key, value ) {
        form.find("#"+key).val(data['model'][key]);
    });

    form.find("#content").val(JSON.stringify(data.userStore));
}

//evento para editar un agente
function editAgentEvent(id_agent){

    let modal = $('#agentModal');
    let form = $("#agentForm");
    populateAgentForm(form, id_agent);

    $('#modalAgentTitle').html('Edit Agent');
    form.unbind("submit").submit(function(e){
        e.preventDefault();
        let form = $(this);
        let adata = JSON.stringify(formAgentToHash(form));
        console.log(adata);
        $.ajax({
            type: 'PATCH',
            url: API_CHAINS_URL,
            data: adata,
            contenType: "application/json",
            success: function(data){
                console.log(data);
                tableAgents();
                clearAgentForm(form);
                let modal = $('#agentModal');
                modal.modal('hide');
            },error: function( jqXHR, textStatus, errorThrown){
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
                showErrors(jqXHR.responseJSON);
            }
        });
    });
    enabled(form.find('#agentName'));
    enabled(form.find('#agentType'));
    enabled(form.find('#providers'));
    enabled(form.find("#toggle_external"));
    enabled(form.find("#urlAgent"));

    let subclase = window.subclasses.find(hash => hash['class'] == form.find("#agentType").val());

    $.each( subclase['modelAttribute'], function( key, value ) {
        enabled(form.find("#"+key));
    });

    $("#formAgentButton").unbind("click").click(function(){
        disabled($('#agentModal .btn'));
        sendEditAgentForm(id_agent);
    });
    modal.modal('show');
}

function sendEditAgentForm(id_agent) {
    let form = $("#agentForm");
    let adata = JSON.stringify(formAgentToHash(form));
    console.log(adata);
    $.ajax({
        type: 'PATCH',
        url: API_AGENTS_URL+ '/' +id_agent,
        data: adata,
        contenType: "application/json",
        success: function(data, status, jqXHR){
            console.log(data);
            console.log(status);
            console.log(jqXHR);
            let msg = data;
            if (status == 'success') {
                $.ajax({
                    type: 'GET',
                    url: API_CHAINS_URL + '/' + window.chain.id + '/agents',
                    success: function(data){
                        alert(msg);
                        console.log(data);
    
                        tableAgents(data);
                        clearAgentForm(form);
                        let modal = $('#agentModal');
                        modal.modal('hide');
                        enabled(form.find('#agentName'));
                        enabled(form.find('#agentType'));
                        enabled(form.find('#providers'));
                        enabled(form.find("#toggle_external"));
                        enabled(form.find("#urlAgent"));
                        let subclase = window.subclasses.find(hash => hash['class'] == form.find("#agentType").val());

                        $.each( subclase['modelAttribute'], function( key, value ) {
                            enabled(form.find("#"+key));
                        });
                        enabled($('#agentModal .btn'));
                    },error: function( jqXHR, textStatus, errorThrown){
                        console.log(jqXHR);
                        console.log(textStatus);
                        console.log(errorThrown);
                        showErrors(jqXHR.responseJSON);
                        enabled($('#agentModal .btn'));
                    }
                });
            } else {
                showErrors(data);
                enabled($('#agentModal .btn'));
            }
        },
        error: function( jqXHR, textStatus, errorThrown){
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
            showErrors(jqXHR.responseJSON);
            enabled($('#agentModal .btn'));
        }
    });
}

//evento para deshabilitar un agente
function removeAgentEvent(id_agent){

    let modal = $('#agentModal');
    let form = $("#agentForm");
    populateAgentForm(form, id_agent);

    $('#modalAgentTitle').html('Are you sure to disable the agent?');
    form.unbind("submit").submit(function(e){
        e.preventDefault();
        let form = $(this);
        let adata = JSON.stringify(formAgentToHash(form));
        console.log(adata);
        $.ajax({
            type: 'PATCH',
            url: API_CHAINS_URL,
            data: adata,
            contenType: "application/json",
            success: function(data){
                console.log(data);
                tableAgents();
                clearAgentForm(form);
                let modal = $('#agentModal');
                modal.modal('hide');
                enabled(form.find('#agentName'));
                enabled(form.find('#agentType'));
                enabled(form.find('#providers'));
                enabled(form.find("#toggle_external"));
                enabled(form.find("#urlAgent"));
            
                let subclase = window.subclasses.find(hash => hash['class'] == form.find("#agentType").val());

                $.each( subclase['modelAttribute'], function( key, value ) {
                    enabled(form.find("#"+key));
                });

            },error: function( jqXHR, textStatus, errorThrown){
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
                showErrors(jqXHR.responseJSON);
            }
        });
    });
    //desabilito inputs para que no editen al agente
    disabled(form.find('#agentName'));
    disabled(form.find('#agentType'));
    form.find('#agentType').trigger('chosen:updated');
    disabled(form.find('#providers'));
    form.find('#providers').trigger('chosen:updated');
    disabled(form.find("#toggle_external"));
    disabled(form.find("#urlAgent"));

    let subclase = window.subclasses.find(hash => hash['class'] == form.find("#agentType").val());

    $.each( subclase['modelAttribute'], function( key, value ) {
        disabled(form.find("#"+key));
    });

    setTimeout(function(){
        form.find('#active').prop('checked', false);
    },500);
    $("#formAgentButton").unbind("click").click(function(){
        sendEditAgentForm(id_agent);
    });
    modal.modal('show');
}

//evento para deshabilitar un agente
function enableAgentEvent(id_agent){

    let modal = $('#agentModal');
    let form = $("#agentForm");
    populateAgentForm(form, id_agent);

    $('#modalAgentTitle').html('Are you sure to enable the agent?');
    form.unbind("submit").submit(function(e){
        e.preventDefault();
        let form = $(this);
        let adata = JSON.stringify(formAgentToHash(form));
        console.log(adata);
        $.ajax({
            type: 'PATCH',
            url: API_CHAINS_URL,
            data: adata,
            contenType: "application/json",
            success: function(data){
                console.log(data);
                tableAgents();
                clearAgentForm(form);
                let modal = $('#agentModal');
                modal.modal('hide');
                enabled(form.find('#agentName'));
                enabled(form.find('#agentType'));
                enabled(form.find('#providers'));
                enabled(form.find("#toggle_external"));
                enabled(form.find("#urlAgent"));
            
                let subclase = window.subclasses.find(hash => hash['class'] == form.find("#agentType").val());

                $.each( subclase['modelAttribute'], function( key, value ) {
                    enabled(form.find("#"+key));
                });
            },error: function( jqXHR, textStatus, errorThrown){
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
                showErrors(jqXHR.responseJSON);
            }
        });
    });
    //desabilito inputs para que no editen al agente
    disabled(form.find('#agentName'));
    disabled(form.find('#agentType'));
    form.find('#agentType').trigger('chosen:updated');
    disabled(form.find('#providers'));
    form.find('#providers').trigger('chosen:updated');
    disabled(form.find("#toggle_external"));
    disabled(form.find("#urlAgent"));

    let subclase = window.subclasses.find(hash => hash['class'] == form.find("#agentType").val());

    $.each( subclase['modelAttribute'], function( key, value ) {
        disabled(form.find("#"+key));
    });

    setTimeout(function(){
        form.find('#active').prop('checked', true);
    },500);
    $("#formAgentButton").unbind("click").click(function(){
        sendEditAgentForm(id_agent);
    });
    modal.modal('show');
}

function toggleURL(form) {
    $('#'+form).find('.divURL').toggle();
}

function goAgentEvent(id_agent) {

    let data = window.agents.find(x => x.id === id_agent);
    if (data !== undefined) {
        $('section').hide();
        window.agent = data;
        $('.agentNameTitle').html(data.agentName);
        $('.agentUriTitle').html(data.uri);
        if (window.chain.template === true) {
            disabled($('#send_message_button'));
            $('#send_message_button').removeAttr('onClick');
        } else {
            $('#send_message_button').attr('onClick','sendMessageFormEvent('+ agent.id +')');
            enabled($('#send_message_button'));
        }
        
        let form = $('#agentSectionForm');
        populateAgentForm(form, agent.id, 'agentSectionForm');
        //refresco conversaciones
        populateConversations(agent.id);


        $('#nav').html('<li class="breadcrumb-item"><a href="#" onClick="goHome();">Home <i class="fa fa-home"></i></a></li><li class="breadcrumb-item"><a href="#" onClick="goChainEvent('+window.chain.id+')">'+window.chain.chainName+' <i class="fa fa-link"></i></a></li><li class="breadcrumb-item active" aria-current="page">'+data.agentName+' <i class="fa fa-user"></i></li>');
        $('#agentSection').fadeIn();
    } else {
        alert('Agent not found!');
    }
}

function reportEvent() {

    if (window.chain !== undefined) {
        $('section').hide();
        $('.reportTitle').html(window.chain.chainName + ' Report');
        
        populateReport();

        $('#nav').html('<li class="breadcrumb-item"><a href="#" onClick="goHome();">Home <i class="fa fa-home"></i></a></li><li class="breadcrumb-item"><a href="#" onClick="goChainEvent('+window.chain.id+')">'+window.chain.chainName+' <i class="fa fa-link"></i></a></li><li class="breadcrumb-item active" aria-current="page">Report <i class="far fa-list-alt"></i></li>');
        $('#reportSection').fadeIn();
    } else {
        alert('Chain not found!');
    }
}

function getSubclassesOfAgents() {
    $.ajax({
        type: 'GET',
        url: API_AGENTS_URL + '/subclassesOfAgents',
        success: function(data, status){
            console.log(data);
            if (status == 'success') {
                window.subclasses = data;
            }else{
                console.log(data);
            }
        },
        error: function( jqXHR, textStatus, errorThrown){
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
            showErrors(jqXHR.responseJSON);
        }
    });
}

//seteo el select de tipos de agente. optionSelected es opcional, solo si tiene un tipo de agente seteados
function selectAgentTypes(form, optionSelected=undefined){
    //vacio el select
    var select = form.find('#agentType').empty();
    //completo los tipos
    $.each(window.subclasses, function(i,item) {
        let key = item['class'];
        select.append( '<option value="'+ key +'">'
                            + key
                        + '</option>' ); 
    });
    //si tiene un tipo lo pongo como selected
    if (optionSelected != undefined && optionSelected != 'undefined') {
        form.find("#agentType option[value='" + optionSelected + "']").prop("selected", true);
        option = window.subclasses.find(hash => hash['class'] == optionSelected);
    }else{
        option = window.subclasses[0];
    }
    form.find("#content").val(JSON.stringify(option['storeAttribute']));
    select.change(function(e){
        let selectedOption = $(this).children("option:selected").val();
        let option = window.subclasses.find(hash => hash['class'] == selectedOption);
        form.find("#content").val(JSON.stringify(option['storeAttribute']));
    });
    //tranformo a chosen y actualizo
    setTimeout(function(){
        select.chosen();
    },500);
    select.trigger('chosen:updated');
}

function inputForModel(attributes){
    let inputs = '';
    $.each( attributes, function( key, value ) {
        switch (value) {
            case 'String':
                inputs += '<div class="col-md-4 form-group">';
                inputs += '<label>' + key +'</label>';
                inputs += '<input type="text" class="form-control" id="' + key +'" name="' + key +'">';
                inputs += '</div>';
                break;
            
            case 'Integer':
                inputs += '<div class="col-md-4 form-group">';
                inputs += '<label>' + key +'</label>';
                inputs += '<input type="number" class="form-control" id="' + key +'" name="' + key +'">';
                inputs += '</div>';
                break;

            case 'Float':
                inputs += '<div class="col-md-4 form-group">';
                inputs += '<label>' + key +'</label>';
                inputs += '<input type="number" class="form-control" id="' + key +'" name="' + key +'" step="0.01">';
                inputs += '</div>';
                break;
        
            default:
                break;
        }
    });
    $(".modelAttributes").empty();
    $(".modelAttributes").append(inputs);
}

/*********************************************************************************************************************************
 * FIN AGENTE
 *********************************************************************************************************************************/

 /********************************************************************************************************************************
  * INICIO MESSAGE
  ********************************************************************************************************************************/

//evento para enviar un mensaje
function sendMessageFormEvent(id_agent){

    let modal = $('#messageModal');
    let form = $("#messageForm");
    clearAgentForm(form);
    populateAgentForm(form, id_agent);
    let data = window.agents.find(x => x.id === id_agent);

    let content = {};
    content['item'] = (data.userStore.products != undefined) ? Object.keys(data.userStore.products)[0] : '';
    content['count'] = (data.userStore.products != undefined) ? Object.values(data.userStore.products)[0].count : '';
    form.find("#content").val(JSON.stringify(content));

    form.unbind("submit").submit(function(e){
        e.preventDefault();
        let form = $(this);
        let agent = window.agents.find(x => x.id === id_agent);
        if (IsJsonString(form.find("#content").val())) {
            let content = JSON.parse(form.find("#content").val());

            let msg = {
                "sender": API_AGENTS_URL + '/0',
                "receiver": agent.uri,
                "content": content,
                "performative": "CallForProposal"
            };
            let data = JSON.stringify(msg);
            console.log(data);
            $.ajax({
                type: 'POST',
                url: agent.uri + '/messages',
                data: data,
                contenType: "application/json",
                success: function(data, status){
                    console.log(data);
                    if (status == 'success') {
                        alert(data);
                    } else {
                        showErrors(data);
                    }
                    modal.modal('hide');
                },error: function( jqXHR, textStatus, errorThrown){
                    console.log(jqXHR);
                    console.log(textStatus);
                    console.log(errorThrown);
                    showErrors(jqXHR.responseJSON);
                }
            });
        } else {
            alert("Invalid content.");
        }
    });
    $("#btn_message").unbind("click").click(function(){
        disabled($('#messageModal .btn'));
        sendMessageForm(id_agent);
    });
    modal.modal('show');
}

function sendMessageForm(id_agent) {
    let form = $("#messageForm");
    let modal = $('#messageModal');
    let agent = window.agents.find(x => x.id === id_agent);
    if (IsJsonString(form.find("#content").val())) {
        let content = JSON.parse(form.find("#content").val());

        let msg = {
            "sender": API_AGENTS_URL + '/0',
            "receiver": agent.uri,
            "content": content,
            "performative": "CallForProposal"
        };
        let data = JSON.stringify(msg);
        console.log(data);
        $.ajax({
            type: 'POST',
            url: agent.uri + '/messages',
            data: data,
            contenType: "application/json",
            success: function(data, status){
                console.log(data);
                console.log(status);
                let msg = data;
                if (status == 'success') {
                    $.ajax({
                        type: 'GET',
                        url: API_CHAINS_URL + '/' + window.chain.id + '/agents',
                        success: function(data){
                            alert(msg);
                            console.log(data);
        
                            tableAgents(data);
                            clearAgentForm(form);
        
                            //refresco conversaciones
                            populateConversations(id_agent);
                            modal.modal('hide');
                            enabled($('#messageModal .btn'));
                        },error: function( jqXHR, textStatus, errorThrown){
                            console.log(jqXHR);
                            console.log(textStatus);
                            console.log(errorThrown);
                            showErrors(jqXHR.responseJSON);
                            enabled($('#messageModal .btn'));
                        }
                    });
                } else {
                    showErrors(data);
                    enabled($('#messageModal .btn'));
                }
            },error: function( jqXHR, textStatus, errorThrown){
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
                showErrors(jqXHR.responseJSON);
                enabled($('#messageModal .btn'));
            }
        });
    }else{
        alert("Invalid content.");
        enabled($('#messageModal .btn'));
    }
}

/*********************************************************************************************************************************
 * FIN MESSAGE
 *********************************************************************************************************************************/

 /********************************************************************************************************************************
  * INICIO CONVERSATION
  ********************************************************************************************************************************/

function populateConversations(id_agent){
    let data = window.agents.find(x => x.id === id_agent);

    $('#conversationCount').html(data.conversations.length);

    let str = '';
    let button = '';
    let pre = '';
    $('#conversationsBody').html('');
    $.each(data.conversations, function(i,item) {
        str = formatMessages(item.messages);
        button = conversationButtonEvent(item.id);
        pre = conversationPreEvent(item.id, str);
        $('#conversationsBody').append(button + pre + '<br><br>');
    });
    $('#conversations').show();
}

function conversationButtonEvent(id_conversation) {
    return '<button class="btn btn-sm btn-primary" onClick="toggleConversation('+ id_conversation +')">Conversation: ' + id_conversation + '</button>';
}

function conversationPreEvent(id_conversation, inp) {
    return '<div id="pre_'+ id_conversation +'" style="display:none; font-size: smaller;"><br>' + inp + '</div>';
}

function toggleConversation(id_conversation) {
    $('#pre_' + id_conversation).toggle();
}

function refreshConversations() {
    $('#spinnerConversation').addClass('fa-spin');
    $.ajax({
        type: 'GET',
        url: API_CHAINS_URL + '/' + window.chain.id + '/agents',
        success: function(data){
            console.log(data);

            tableAgents(data);

            //refresco conversaciones
            populateConversations(window.agent.id);
            $('#spinnerConversation').removeClass('fa-spin');
        },error: function( jqXHR, textStatus, errorThrown){
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
            showErrors(jqXHR.responseJSON);
            $('#spinnerConversation').removeClass('fa-spin');
        }
    });
}

function formatMessage(message) {

    let html = '';

    if ( message != undefined && message != 'undefined' ) {
        
        let bg = '';
        switch (message.performative) {
            case 'CallForProposal':
                bg = 'text-white bg-primary';
                break;
            
            case 'Propose':
                bg = 'text-white bg-secondary';
                break;
    
            case 'RejectProposal':
                bg = 'text-white bg-warning';
                break;
    
            case 'AcceptProposal':
                bg = 'bg-light';
                break;
    
            case 'Refuse':
                bg = 'text-white bg-dark';
                break;
    
            case 'Failure':
                bg = 'text-white bg-danger';
                break;
    
            case 'InformDone':
                bg = 'text-white bg-success';
                break;
    
            case 'InformResult':
                bg = 'text-white bg-info';
                break;
        
            default:
                break;
        }
    
        let performative = ( message.performative != undefined 
                        && message.performative != 'undefined' ) ? message.performative : '';
    
        let item = ( message.content != undefined 
                    && message.content != 'undefined'
                    && message.content.item != undefined 
                    && message.content.item != 'undefined' ) ? message.content.item : '';
    
        let sender = window.agents.find(x => x.uri === message.sender);
        let receiver = window.agents.find(x => x.uri === message.receiver);
    
        sender = ( sender != undefined 
                && sender != 'undefined' 
                && sender.agentName != undefined 
                && sender.agentName != 'undefined' ) ? sender.agentName : message.sender;
        sender = ( sender == API_AGENTS_URL + '/0' ) ? 'You' : sender;

        receiver = ( receiver != undefined 
                && receiver != 'undefined' 
                && receiver.agentName != undefined 
                && receiver.agentName != 'undefined' ) ? receiver.agentName : message.receiver;
        receiver = ( receiver == API_AGENTS_URL + '/0' ) ? 'You' : receiver;
    
        html += '<div class="card col-md-2 '+ bg + ' mb-3 mr-1"><div class="card-header"><strong>'+ performative +'</strong></div><div class="card-body"><p class="card-text"><strong>Sender:</strong> '+ sender +'<br><strong>Receiver:</strong> '+ receiver +'<br>';
        
        if  ( message.content != undefined && message.content != 'undefined' ) {
            
            Object.keys(message.content).forEach(key => {
                html += '<strong>'+key+':</strong> '+ message.content[key] +'<br>';
            });

        }
    
        html += '</p></div></div>';

    }
    
    return html;
}

function formatMessages(messages) {
    let messagesHTML = '<div class="row ml-1">';
    $.each(messages, function(i,message){
        messagesHTML += formatMessage(message);
    });
    messagesHTML += '</div>';
    return messagesHTML;
}

//aplico estilo al texto en json
function syntaxHighlight(json) {
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        var cls = 'number';
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'key';
            } else {
                cls = 'string';
            }
        } else if (/true|false/.test(match)) {
            cls = 'boolean';
        } else if (/null/.test(match)) {
            cls = 'null';
        }
        return '<span class="' + cls + '">' + match + '</span>';
    });
}

 /********************************************************************************************************************************
  * INICIO REPORT
  ********************************************************************************************************************************/

 function populateReport(){
    $('#reportSection .box-body').append('<div class="loading text-center"><h5><i class="fa fa-cog fa-spin fa-3x fa-fw"></i><div class="">Loading...</div></h5></div>');
    //busco las conversaciones de la cadena
    $.ajax({
        type: 'GET',
        url: API_CHAINS_URL + '/' + window.chain.id + '/conversations',
        success: function(data){
            console.log(data);
            $('#reportCount').html(data.length);
            //let str = '';
            let button = '';
            let pre = '';
            $('#reportsBody').html('');
            $.each(data, function(i,id) {
                button = reportButtonEvent(id);
                pre = reportPreEvent(id);
                $('#reportsBody').append(button + pre + '<br><br>');
            });
            $('#reportSection .loading').remove();
            $('#reports').show();
        },
        error: function( jqXHR, textStatus, errorThrown){
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
            showErrors(jqXHR.responseJSON);
            $('#reportSection .loading').remove();
        }
    });

}

function reportButtonEvent(id) {
    return '<button class="btn btn-sm btn-primary" onClick="toggleReport('+ id +')">Conversation: ' + id + '</button>';
}

function reportPreEvent(id) {
    return '<div id="pre_report_'+ id +'" class="card" style="display:none;"><br></div>';
}

function toggleReport(id) {
    $('#pre_report_' + id).toggle();
    if ($('#pre_report_' + id).is(":visible")) {
        $('#pre_report_' + id).html('<div class="loading text-center"><h5><i class="fa fa-cog fa-spin fa-3x fa-fw"></i><div class="">Loading...</div></h5></div>');
        //busco el reporte de la conversacion
        $.ajax({
            type: 'GET',
            url: API_CHAINS_URL + '/' + window.chain.id + '/conversations/' + id + '/report',
            success: function(data){
                console.log(data);
                let report = '';
                let val = '';
                $.each(data, function(i,element) {
                    val = (element.value != null && element.value != undefined) ? element.value : '';
                    report += '<strong>' + element.key + ': </strong>' + val + '<br>';
                });
                $('#pre_report_' + id + ' .loading').remove();
                $('#pre_report_' + id).append('<div class="card-body">' + report + '</div>');
            },
            error: function( jqXHR, textStatus, errorThrown){
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
                showErrors(jqXHR.responseJSON);
                $('#pre_report_' + id).toggle();
                $('#pre_report_' + id + ' .loading').remove();
            }
        });
    }
}
