Simulación de cadena de suministros
================================

Framework para simplificar la construcción del modelado y simulación de cadenas de suministros. 

Instrucciones
================================

## Instalar serverless
Vía npm:

```npm install -g serverless```

## AWS credenciales
Para su uso se necesita configurar las credenciales de aws. Pasos para obtener las misma:

1. Abra la consola de [IAM](https://console.aws.amazon.com/iam/home?#/home).
2. En el panel de navegación de la consola, elija Usuarios.
3. Seleccione su nombre de usuario de IAM (no la casilla de verificación).
4. Elija la pestaña Security credentials y, a continuación, seleccione Create access key.
5. Para ver la nueva clave de acceso, elija Show. Sus credenciales tendrán el aspecto siguiente:
	* ID de clave de acceso: AKIAIOSFODNN7EXAMPLE
	* Clave de acceso secreta: wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY
6. Para descargar el par de claves, elija Download .csv file. Almacene las claves en un lugar seguro.

#### Configurar credenciales

```serverless config credentials --provider aws --key xx --secret yyy```

En este [video](https://www.youtube.com/watch?v=HSd9uYj2LJA) se explica cómo obtener y configurar las credenciales.

## Instalar el framework
Clonar el proyecto.
	
``` git clone https://bitbucket.org/estebans16/simulation_supply_chain.git ```


TODO ver base de dato